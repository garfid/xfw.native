# This file is part of the XVM Framework project.
#
# Copyright (c) 2017-2019 XVM Team.
#
# XVM Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# XVM Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

Push-Location $PSScriptRoot 

Import-Module ./build_lib/library.psm1 -Force -DisableNameChecking

function Build-CmakeProject ($Path, $Config = "RelWithDebInfo", $Arch = "Win32", $Defines = $null) 
{
    $name = Split-Path -Path $Path -Leaf
    Write-Output "  * $name"

    New-Item -ItemType Directory -Path ./output/build/$Arch/$name/ | Out-Null
  
    $root = (Get-Location).Path -replace "\\","/"
    Push-Location "$root/output/build//$Arch/$name/"
    
    cmake -T v141_xp -A $Arch "$root/$Path" -DCMAKE_INSTALL_PREFIX="$root/output/devel/$Arch" -DCMAKE_PREFIX_PATH="$root/output/devel/$Arch" $Defines
    if ($LastExitCode -ne 0) {
        Pop-Location
        exit $LastExitCode
    }
    
    cmake --build . --target INSTALL --config $Config
    if ($LastExitCode -ne 0) {
        Pop-Location
        exit $LastExitCode
    }

    Pop-Location
}

function Build-Python()
{
    Build-PythonFile -FilePath "./src/wot_python/__empty__.py"  -OutputDirectory "./output/python/res/mods/xfw_packages/xfw_native/"
    Move-Item "./output/python/res/mods/xfw_packages/xfw_native/__empty__.pyc" "./output/python/res/mods/xfw_packages/xfw_native/__init__.pyc" -Force

    Build-PythonFile -FilePath "./src/wot_python/__init__.py"  -OutputDirectory "./output/python/res/mods/xfw_packages/xfw_native/python/"

    #HTTP Stack
    #Build-PythonFile -FilePath "./src/cpython_python/ssl.py" -OutputDirectory "./output/python/res/mods/xfw_libraries/"
    #Build-PythonFile -FilePath "./src/cpython_python/httplib.py" -OutputDirectory "./output/python/res/mods/xfw_libraries/"
    #Build-PythonFile -FilePath "./src/cpython_python/urllib.py" -OutputDirectory "./output/python/res/mods/xfw_libraries/"
    #Build-PythonFile -FilePath "./src/cpython_python/urllib2.py" -OutputDirectory "./output/python/res/mods/xfw_libraries/"

    #SQLite3
    Build-PythonFile -FilePath "./src/cpython_python/sqlite3/__init__.py" -OutputDirectory "./output/python/res/mods/xfw_libraries/sqlite3/"
    Build-PythonFile -FilePath "./src/cpython_python/sqlite3/dbapi2.py"   -OutputDirectory "./output/python/res/mods/xfw_libraries/sqlite3/"
    Build-PythonFile -FilePath "./src/cpython_python/sqlite3/dump.py"     -OutputDirectory "./output/python/res/mods/xfw_libraries/sqlite3/"
}

function Build-Wotmod()
{
    Copy-Item -Path "./output/python/res/" -Destination "./output/wotmod/res" -Force -Recurse

    New-Item -Path "./output/wotmod/res/mods/xfw_packages/xfw_native/native_32bit/" -ItemType Directory -ErrorAction SilentlyContinue | Out-Null
    New-Item -Path "./output/wotmod/res/mods/xfw_packages/xfw_native/native_64bit/" -ItemType Directory -ErrorAction SilentlyContinue | Out-Null

    Copy-Item -Path "./output/devel/Win32/bin/*" -Destination "./output/wotmod/res/mods/xfw_packages/xfw_native/native_32bit/" -Recurse
    Copy-Item -Path "./output/devel/x64/bin/*" -Destination "./output/wotmod/res/mods/xfw_packages/xfw_native/native_64bit/" -Recurse
    Copy-Item -Path "./LICENSE.md"         -Destination "./output/wotmod/LICENSE.md" 

    $xfwVersion = Get-Content "./src/meta/version.txt"

    (Get-Content "./src/meta/meta.xml.in").Replace("XFW_VERSION","${xfwVersion}") | Set-Content "./output/wotmod/meta.xml"
    (Get-Content "./src/meta/xfw_package.json").Replace("XFW_VERSION","${xfwVersion}") | Set-Content "./output/wotmod/res/mods/xfw_packages/xfw_native/xfw_package.json"

    Create-Zip -Directory "./output/wotmod/"

    Move-Item "./output/wotmod/output.zip" "./output/deploy/com.modxvm.xfw.native_${xfwVersion}.wotmod" -Force
}

function Build-Zip()
{
        Copy-Item -Path "./output/python/res/mods/" -Destination "./output/unpacked/res_mods/" -Force -Recurse
        
        New-Item -Path "./output/unpacked/res_mods/mods/xfw_packages/xfw_native/native_32bit/" -ItemType Directory -ErrorAction SilentlyContinue | Out-Null
        New-Item -Path "./output/unpacked/res_mods/mods/xfw_packages/xfw_native/native_64bit/" -ItemType Directory -ErrorAction SilentlyContinue | Out-Null
    
        Copy-Item -Path "./output/devel/Win32/bin/*" -Destination "./output/unpacked/res_mods/mods/xfw_packages/xfw_native/native_32bit/" -Recurse
        Copy-Item -Path "./output/devel/x64/bin/*"   -Destination "./output/unpacked/res_mods/mods/xfw_packages/xfw_native/native_64bit/" -Recurse
        Copy-Item -Path "./LICENSE.md"         -Destination "./output/unpacked/LICENSE.md" 
    
        $xfwVersion = Get-Content "./src/meta/version.txt"
    
        (Get-Content "./src/meta/xfw_package.json").Replace("XFW_VERSION","${xfwVersion}") | Set-Content "./output/unpacked/res_mods/mods/xfw_packages/xfw_native/xfw_package.json"
    
        Create-Zip -Directory "./output/unpacked/"
    
        Move-Item "./output/unpacked/output.zip" "./output/deploy/com.modxvm.xfw.native_${xfwVersion}.zip" -Force
}

function Build-DevelPackage()
{
    $xfwVersion = Get-Content "./src/meta/version.txt"

    Create-Zip -Directory "./output/devel/" -CompressionLevel 9
    Move-Item "./output/devel/output.zip" "./output/deploy/com.modxvm.xfw.native_${xfwVersion}-devel.zip" -Force
}

function Build-Libraries()
{
    Build-CmakeProject -Path ./depends/audiokinetic -Arch Win32
    Build-CmakeProject -Path ./depends/audiokinetic -Arch x64

    Build-CmakeProject -Path ./depends/minhook -Arch Win32
    Build-CmakeProject -Path ./depends/minhook -Arch x64

    Build-CmakeProject -Path ./src/lib_python -Arch Win32
    Build-CmakeProject -Path ./src/lib_python -Arch x64

    Build-CmakeProject -Path ./src/lib_wotexport -Arch Win32
    Build-CmakeProject -Path ./src/lib_wotexport -Arch x64

    Build-CmakeProject -Path ./src/lib_d3dhook -Arch Win32
    Build-CmakeProject -Path ./src/lib_d3dhook -Arch x64

    Build-CmakeProject -Path ./depends/pybind11 -Arch Win32 -Defines ("-DPYTHONLIBS_FOUND=TRUE", "-DPYTHON_MODULE_EXTENSION="".pyd""", "-DPYBIND11_TEST=OFF")
    Build-CmakeProject -Path ./depends/pybind11 -Arch x64 -Defines ("-DPYTHONLIBS_FOUND=TRUE", "-DPYTHON_MODULE_EXTENSION="".pyd""", "-DPYBIND11_TEST=OFF")

    Build-CmakeProject -Path ./src/cpython_cjkcodecs -Arch Win32
    #Build-CmakeProject -Path ./src/cpython_cjkcodecs -Arch x64

    Build-CmakeProject -Path ./src/cpython_ctypes -Arch Win32
    #Build-CmakeProject -Path ./src/cpython_ctypes -Arch x64

    Build-CmakeProject -Path ./src/cpython_sqlite -Arch Win32
    #Build-CmakeProject -Path ./src/cpython_sqlite -Arch x64
}

Remove-Item -Path ./output/  -Force -Recurse -ErrorAction SilentlyContinue
New-Item -ItemType Directory -Path ./output/build/  | Out-Null
New-Item -ItemType Directory -Path ./output/deploy/ | Out-Null
New-Item -ItemType Directory -Path ./output/logs/ | Out-Null

Write-Output "Building python"
Build-Python
Write-Output ""

Write-Output "Building libraries"
Build-Libraries

#temporary disabled
#Build-CmakeProject -Name ./src/cpython_ssl

Write-Output ""


Write-Output "Building archives"

Write-Output "  * .wotmod"
Build-Wotmod

Write-Output "  * .zip"
Build-Zip

Write-Output "  * developer package"
Build-DevelPackage

Write-Output ""

Pop-Location
