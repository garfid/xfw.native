# This file is part of the XVM project.
#
# Copyright (c) 2017-2019 XVM contributors.
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required (VERSION 3.9)

project(audiokinetic)

include(CMakePackageConfigHelpers)

set(AUDIOKINETIC_MAJOR_VERSION 2017)
set(AUDIOKINETIC_MINOR_VERSION 2)
set(AUDIOKINETIC_PATCH_VERSION 5)
set(AUDIOKINETIC_VERSION ${AUDIOKINETIC_MAJOR_VERSION}.${AUDIOKINETIC_MINOR_VERSION}.${AUDIOKINETIC_PATCH_VERSION})

################
#    BUILD     # 
################

add_library(audiokinetic INTERFACE)  

target_include_directories(audiokinetic INTERFACE 
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
)

################
# CMAKE CONFIG # 
################

configure_package_config_file(
  "cmake/audiokinetic-config.cmake.in"
  "audiokinetic-config.cmake"
INSTALL_DESTINATION 
  "lib/audiokinetic"
)

write_basic_package_version_file(
  "audiokinetic-config-version.cmake"
  VERSION 
    ${AUDIOKINETIC_VERSION}
  COMPATIBILITY
    AnyNewerVersion
)

install(
  FILES 
    "${CMAKE_CURRENT_BINARY_DIR}/audiokinetic-config.cmake"
    "${CMAKE_CURRENT_BINARY_DIR}/audiokinetic-config-version.cmake"
  DESTINATION 
    "lib/audiokinetic"
)

###################
#     INSTALL     #
###################

install(TARGETS audiokinetic
  EXPORT audiokinetic-targets
  RUNTIME DESTINATION "bin"
  ARCHIVE DESTINATION "lib"
  LIBRARY DESTINATION "lib"
)

install(
  EXPORT
    audiokinetic-targets
  NAMESPACE
    audiokinetic::
  DESTINATION 
    "lib/audiokinetic"
)

install(
  DIRECTORY include DESTINATION .
)
