# XVM Framework Native Components

[![Build status](https://ci.appveyor.com/api/projects/status/x26vy8imdn3ku027?svg=true)](https://ci.appveyor.com/project/MikhailPaulyshka/xfw-native)

C/C++ components for XVM Framework.

## Content

* wrappers
    * audiokinetic - WWISE wrapper for WoT
    * python - libpython wrapper for WoT
* cpython modules
    * cjkcodecs - Chinese, Japanese and Korean character sets support for Python
    * ctypes - a foreign function library for Python
* 3rdparty libraries
    * minhook - minimalistic API Hooking Library
* tools
    * IDA importer - auto functions renaming in IDA


## Requirements

* Microsoft Windows 7+
* Microsoft Visual Studio 2015/2017 with Windows XP target support
* CMake 3.9+


## How to build

* run `./build.ps1`
* grab output in `/output/deploy/` directory

