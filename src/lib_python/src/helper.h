/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2019 XVM contributors.
* Copyright (c) 2018-2019 Yuki Koyama
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <thread>
#include <vector>

class Helper 
{
public:
	Helper() = delete;
	~Helper() = delete;

	static size_t GetModuleSize(const wchar_t* filename);

	static bool DataCompare(const char* pData, const char* bMask, const char* szMask);

	static size_t FindFunction(size_t startpos, size_t endpos, const char* pattern, const char* mask);

	static size_t Read32BitFromAddress(size_t address);

	static  bool ValidateStructure(size_t address, const char* validatePattern, const char* validateMask);

	/// \brief Execute a for-loop process for an array in parallel
	/// \param n The number of iterations. I.e., { 0, 1, ..., n - 1 } will be visited.
	/// \param function The function that will be called in the for-loop. This can be specified as a lambda expression. The type should be equivalent to std::function<void(int)>.
	/// \param target_concurrency The number of threads that will be generated. When this is set to zero (which is the default), the hardware concurrency will be automatically used.
	template<typename Callable>
	static void ParallelFor(int n, Callable function, int target_concurrency = 0);

};

template<typename Callable>
inline void Helper::ParallelFor(int n, Callable function, int target_concurrency)
{
	std::vector<std::thread> threads;

	const int hint = (target_concurrency == 0) ? std::thread::hardware_concurrency() : target_concurrency;
	const int thread_count = std::min(n, (hint == 0) ? 4 : hint);

	const int n_max_tasks_per_thread = (n / thread_count) + (n % thread_count == 0 ? 0 : 1);
	const int n_lacking_tasks = n_max_tasks_per_thread * thread_count - n;

	auto inner_loop = [&](const int thread_index) {
		const int n_lacking_tasks_so_far = std::max(thread_index - thread_count + n_lacking_tasks, 0);
		const int inclusive_start_index = thread_index * n_max_tasks_per_thread - n_lacking_tasks_so_far;
		const int exclusive_end_index = inclusive_start_index + n_max_tasks_per_thread - (thread_index - thread_count + n_lacking_tasks >= 0 ? 1 : 0);

		for (auto k = inclusive_start_index; k < exclusive_end_index; ++k)
		{
			function(k);
		}
	};

	for (auto j = 0; j < thread_count; ++j) {
		threads.push_back(std::thread(inner_loop, j));
	}

	for (auto& t : threads) {
		t.join();
	}
}
