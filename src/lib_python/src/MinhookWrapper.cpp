/**
* This file is part of the WoT libpython project.
*
* Copyright (c) 2017 WoT libpython contributors.
*
* WoT libpython is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* WoT libpython is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <Windows.h>
#include <Shlwapi.h>

#include <MinHook.h>

#include "Libpython.h"

static HMODULE hMinhook = NULL;

typedef MH_STATUS(WINAPI *MHINITIALIZE)(VOID);
typedef MH_STATUS(WINAPI *MHUNINITIALIZE)(VOID);
typedef MH_STATUS(WINAPI *MHCREATEHOOK)(LPVOID, LPVOID, LPVOID *);
typedef MH_STATUS(WINAPI *MHENABLEHOOK)(LPVOID);
typedef MH_STATUS(WINAPI *MHDISABLEHOOK)(LPVOID);

#define MAX_PATH 260

MH_STATUS WINAPI MH_Initialize(VOID)
{
	WCHAR Path[MAX_PATH];
	GetModuleFileNameW(PyWin_DLLhModule, Path, MAX_PATH);
	PathRemoveFileSpecW(Path);

#ifdef _WIN64
	wcsncat(Path, L"\\MinHook.x64.dll", 16);
#else
	wcsncat(Path, L"\\MinHook.x32.dll", 16);
#endif

	hMinhook = LoadLibraryW(Path);
	if (hMinhook == NULL)
		return MH_ERROR_MODULE_NOT_FOUND;

	MHINITIALIZE MH_Initialize_real = (MHINITIALIZE)GetProcAddress(hMinhook, "MH_Initialize");
	return MH_Initialize_real();
}

MH_STATUS WINAPI MH_CreateHook(LPVOID pTarget, LPVOID pDetour, LPVOID *ppOriginal)
{
	if(hMinhook==NULL)
		return MH_ERROR_NOT_INITIALIZED;

	MHCREATEHOOK MH_CreateHook_real = (MHCREATEHOOK)GetProcAddress(hMinhook, "MH_CreateHook");
	return MH_CreateHook_real(pTarget, pDetour, ppOriginal);
}

MH_STATUS WINAPI MH_EnableHook(LPVOID pTarget)
{
	if(hMinhook==NULL)
		return MH_ERROR_NOT_INITIALIZED;

	MHENABLEHOOK MH_EnableHook_real = (MHENABLEHOOK)GetProcAddress(hMinhook, "MH_EnableHook");
	return MH_EnableHook_real(pTarget);
}

MH_STATUS WINAPI MH_DisableHook(LPVOID pTarget)
{
	if(hMinhook==NULL)
		return MH_ERROR_NOT_INITIALIZED;

	MHDISABLEHOOK MH_DisableHook_real = (MHDISABLEHOOK)GetProcAddress(hMinhook, "MH_DisableHook");
	return MH_DisableHook_real(pTarget);
}

MH_STATUS WINAPI MH_Uninitialize(VOID)
{
	if(hMinhook==NULL)
		return MH_ERROR_NOT_INITIALIZED;

	MHUNINITIALIZE MH_Uninitialize_real = (MHUNINITIALIZE)GetProcAddress(hMinhook, "MH_Uninitialize");
	return MH_Uninitialize_real();
}
