/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2019 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

class Initializer {
public:
	Initializer() = delete;
	~Initializer() = delete;

	static bool Initialize();

	static size_t GetFunctionRealAddress(const char* str);

	static size_t GetStructureRealAddress(const char* str);

	static size_t GetRealAddress(const char* str);


private:
	static bool findFunctionsParallel();

	static bool findFunctionsSequential();

	static bool findFunction(size_t startPosition, size_t endPosition, int functionId);

	static bool findStructures();

	static bool loadCache();

	static bool saveCache();
};
