#include "XfwNativeApi.h"

#include "Helper.h"
#include "Initializer.h"

size_t XFWNATIVE_GetRealAddress(const char* name) {
	return Initializer::GetRealAddress(name);
}

size_t XFWNATIVE_GetModuleSize(const wchar_t* name)
{
	return Helper::GetModuleSize(name);
}

size_t XFWNATIVE_FindFunction(size_t startpos, size_t endpos, const char* pattern, const char* mask)
{
	return Helper::FindFunction(startpos, endpos, pattern, mask);
}
