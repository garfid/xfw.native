/**
* This file is part of the XVM project.
*
* Copyright (c) 1991-2014 python contributors
* Copyright (c) 2017-2019 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#define Py_BUILD_CORE 1

#include <Windows.h>
#include <Python.h>

#include "Libpython.h"
#include "FixSysobject.h"

bool FixSysobject::Fix()
{
	PyObject* v, * m, * sysdict;

	m = PyImport_AddModule("sys");
	sysdict = PyModule_GetDict(m);

	#define SET_SYS_FROM_STRING(key, value)                 \
        v = value;                                          \
        if (v != NULL)                                      \
            PyDict_SetItemString(sysdict, key, v);          \
        Py_XDECREF(v)

	SET_SYS_FROM_STRING("dllhandle", PyLong_FromVoidPtr(PyWin_DLLhModule));
	SET_SYS_FROM_STRING("winver", PyString_FromString(PyWin_DLLVersionString));

	return true;
}
