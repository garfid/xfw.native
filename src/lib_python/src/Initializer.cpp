/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2019 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdio>

#include "CacheFile.h"
#include "Helper.h"
#include "Initializer.h"

//codegen
#include "libpython_data.h"
#include "libpython_trampolines_func.h"
#include "libpython_trampolines_struct.h"

bool Initializer::Initialize()
{
	if (!loadCache())
	{
		if (!findFunctionsParallel()) {
			return false;
		}

		if (!findStructures()) {
			return false;
		}

		WOTPYTHON_FillStructuresTrampolines();
		saveCache();
	}

	return true;
}

size_t Initializer::GetFunctionRealAddress(const char* str)
{
	if (str == nullptr) {
		return 0;
	}

	for (size_t i = 0; i < Functions_Count; i++) {
		if (strcmp(str, Functions_Names[i]) == 0) {
			return Functions_Addrs[i];
		}
	}

	return 0;
}

size_t Initializer::GetStructureRealAddress(const char* str)
{
	if (str == nullptr) {
		return 0;
	}

	for (size_t i = 0; i < Structures_Count; i++) {
		if (strcmp(str, Structures_Names[i]) == 0) {
			return Structures_Addrs[i];
		}
	}

	return 0;
}

size_t Initializer::GetRealAddress(const char* str)
{
	size_t result = 0;

	if (str == nullptr) {
		return result;
	}

	result = GetFunctionRealAddress(str);
	if (result == 0) {
		result = GetStructureRealAddress(str);
	}

	return result;
}

bool Initializer::findFunctionsParallel()
{
	bool success = true;

	//get filename
	WCHAR lpFilename[2048];
	GetModuleFileNameW(nullptr, lpFilename, sizeof(lpFilename));

	//get addr
	size_t startpos = reinterpret_cast<size_t>(GetModuleHandleW(lpFilename));
	size_t endpos = startpos + Helper::GetModuleSize(lpFilename);

	//fill functions
	Helper::ParallelFor(Functions_Count, [=, &success](int functionId) {
		if (!findFunction(startpos, endpos, functionId)) {
			success = false;
		}
	});

	return success;
}

bool Initializer::findFunctionsSequential()
{
	bool success = true;

	//get filename
	WCHAR lpFilename[2048];
	GetModuleFileNameW(nullptr, lpFilename, sizeof(lpFilename));

	//get addr
	size_t startpos = reinterpret_cast<size_t>(GetModuleHandleW(lpFilename));
	size_t endpos = startpos + Helper::GetModuleSize(lpFilename);

	//fill functions
	for (int functionId = 0; functionId < Functions_Count; functionId++) {
		if (!findFunction(startpos, endpos, functionId)) {
			success = false;
		}
	}

	return success;
}

bool Initializer::findFunction(size_t startPosition, size_t endPosition, int functionId)
{
	size_t addr = 0;

	for (size_t sig_id = 0; sig_id < Functions_SignatureCount[functionId]; sig_id++)
	{
		addr = Helper::FindFunction(startPosition, endPosition, Functions_SignatureData[functionId][sig_id], Functions_SignatureMask[functionId][sig_id]);
		if (addr != 0) {
			Functions_Addrs[functionId] = addr;
			break;
		}
	}

	if (addr == 0) {
		char str[255]{};
		snprintf(str, 255, "XFW.Native/libpython: function not found: %s", Functions_Names[functionId]);
		OutputDebugStringA(str);
		return false;
	}

	return true;
}

bool Initializer::findStructures()
{
	bool success = true;

	for (size_t struct_id = 0; struct_id < Structures_Count; struct_id++)
	{
		size_t addr = 0;

		for (size_t sig_id = 0; sig_id < Structures_SignatureCount[struct_id]; sig_id++)
		{
			auto function_addr = Functions_Addrs[Structures_SignatureFunctionIndex[struct_id][sig_id]];
			auto function_offset = Structures_SignatureOffset[struct_id][sig_id];
			addr = Helper::Read32BitFromAddress(function_addr + function_offset);

			if (addr == 0) {
				continue;
			}
			
#ifdef _WIN64
			addr += function_addr + function_offset + 4;
#endif

			if (Helper::ValidateStructure(
				Functions_Addrs[Structures_SignatureFunctionIndex[struct_id][sig_id]] + Structures_SignatureOffset[struct_id][sig_id],
				Structures_SignatureValidationPrefixData[struct_id][sig_id],
				Structures_SignatureValidationPrefixMask[struct_id][sig_id]) == FALSE) {
				addr = 0;
				continue;
			}

			Structures_Addrs[struct_id] = addr;
			break;
		}

		if (addr == 0) {
			char str[255]{};
			snprintf(str, 255, "XFW.Native/libpython: structure not found: %s", Structures_Names[struct_id]);
			OutputDebugStringA(str);
			success = false;
		}
	}

	return success;
}

bool Initializer::loadCache()
{
	auto* file = CacheFile::SearchInCacheFolder();
	if (file != nullptr) {
		file->Apply();
		return true;
	}

	return false;
}

bool Initializer::saveCache()
{
	auto* file = new CacheFile();
	file->Serialize();

	return true;
}
