/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2019 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <filesystem>
#include <string>
#include <vector>

class CacheFile
{
public:
    CacheFile();

    static CacheFile* SearchInCacheFolder();
    void Serialize();

    void Apply();

private:
    CacheFile(const  std::filesystem::path& FilePath);

    void Deserialize(const std::filesystem::path& FilePath);
    void Serialize(const std::filesystem::path& FilePath);

    static std::filesystem::path GetApplicationFilePath();
    static std::filesystem::path GetDllFilePath();
    static std::vector<uint8_t> CalculateSha1OfFile(const std::filesystem::path& FilePath);

    std::vector<uint8_t> sha1Application;
    std::vector<uint8_t> sha1Dll;

    std::vector<size_t> addrsFunctions;
    std::vector<size_t> addrsStructures;

    static inline std::filesystem::path search_directory_32 = L"../mods/temp/com.modxvm.xfw.native/cache_32bit/";
	static inline std::filesystem::path search_directory_64 = L"../mods/temp/com.modxvm.xfw.native/cache_64bit/";
};
