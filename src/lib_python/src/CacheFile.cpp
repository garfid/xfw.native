/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2019 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <fstream>

#include <Windows.h>

#include "CacheFile.h"

#include "digestpp.hpp"
#include "libpython.h"
#include "libpython_data.h"
#include "libpython_trampolines_func.h"
#include "libpython_trampolines_struct.h"

CacheFile::CacheFile()
{
    addrsFunctions = std::vector<size_t>(std::begin(Functions_Addrs), std::end(Functions_Addrs));
    addrsStructures = std::vector<size_t>(std::begin(Structures_Addrs), std::end(Structures_Addrs));
    sha1Application = CalculateSha1OfFile(GetApplicationFilePath());
    sha1Dll = CalculateSha1OfFile(GetDllFilePath());
}

CacheFile::CacheFile(const std::filesystem::path& FilePath)
{
    Deserialize(FilePath);
}

CacheFile* CacheFile::SearchInCacheFolder()
{
    const auto sha1Application = CalculateSha1OfFile(GetApplicationFilePath());
    const auto sha1Dll = CalculateSha1OfFile(GetDllFilePath());

#ifdef _WIN64
	auto cache_dir = GetApplicationFilePath().parent_path() / search_directory_64;
#else
	auto cache_dir = GetApplicationFilePath().parent_path() / search_directory_32;
#endif
	
	if (!std::filesystem::exists(cache_dir)) {
		return nullptr;
	}

    for(auto& file: std::filesystem::directory_iterator(cache_dir))
    {
        auto* f = new CacheFile(file);
        if (f->sha1Dll == sha1Dll && f->sha1Application == sha1Application)
            return f;
    }

    return nullptr;
}

void CacheFile::Serialize()
{
    std::wostringstream oss;
    for (size_t i = 0; i < sha1Dll.size() / 2;i++)  
        oss << std::hex << std::setw(2) << std::setfill(L'0') << sha1Dll[i];
    for (size_t i = 0; i < sha1Application.size() / 2; i++)
        oss << std::hex << std::setw(2) << std::setfill(L'0') << sha1Application[i];


#ifdef _WIN64
	auto filepath = GetApplicationFilePath().parent_path() / search_directory_64 / oss.str();
#else
	auto filepath = GetApplicationFilePath().parent_path() / search_directory_32 / oss.str();
#endif

    Serialize(filepath);
}

void CacheFile::Apply()
{
    memcpy(Functions_Addrs, addrsFunctions.data(), addrsFunctions.size() * sizeof(size_t));
    memcpy(Structures_Addrs, addrsStructures.data(), addrsStructures.size() * sizeof(size_t));
    WOTPYTHON_FillStructuresTrampolines();
}

void CacheFile::Deserialize(const std::filesystem::path& FilePath)
{
    std::ifstream file(FilePath, std::ios::in | std::ios::binary);
   
    sha1Dll.clear();
    sha1Dll.resize(20);
    file.read(reinterpret_cast<char*>(sha1Dll.data()),20);
    if (sha1Dll != CalculateSha1OfFile(GetDllFilePath())) {
        return;
    }

    sha1Application.clear();
    sha1Application.resize(20);
    file.read(reinterpret_cast<char*>(sha1Application.data()), 20);

    addrsFunctions.clear();
    addrsFunctions.resize(Functions_Count);
    file.read(reinterpret_cast<char*>(addrsFunctions.data()), Functions_Count*sizeof(size_t));
    
    addrsStructures.clear();
    addrsStructures.resize(Structures_Count);
    file.read(reinterpret_cast<char*>(addrsStructures.data()), Structures_Count*sizeof(size_t));

    file.close();
}

void CacheFile::Serialize(const std::filesystem::path& FilePath)
{
	if (!std::filesystem::exists(FilePath.parent_path().parent_path().parent_path().parent_path().parent_path()))
		std::filesystem::create_directory(FilePath.parent_path().parent_path().parent_path().parent_path().parent_path());

    if (!std::filesystem::exists(FilePath.parent_path().parent_path().parent_path().parent_path()))
        std::filesystem::create_directory(FilePath.parent_path().parent_path().parent_path().parent_path());

    if (!std::filesystem::exists(FilePath.parent_path().parent_path().parent_path()))
        std::filesystem::create_directory(FilePath.parent_path().parent_path().parent_path());

    if (!std::filesystem::exists(FilePath.parent_path().parent_path()))
        std::filesystem::create_directory(FilePath.parent_path().parent_path());

    if (!std::filesystem::exists(FilePath.parent_path()))
        std::filesystem::create_directory(FilePath.parent_path());

    std::ofstream file(FilePath, std::ios::out | std::ios::binary);
    file.write(reinterpret_cast<char*>(sha1Dll.data()), sha1Dll.size() * sizeof(uint8_t));
    file.write(reinterpret_cast<char*>(sha1Application.data()), sha1Application.size() * sizeof(uint8_t));
    file.write(reinterpret_cast<char*>(addrsFunctions.data()), addrsFunctions.size() * sizeof(size_t));
    file.write(reinterpret_cast<char*>(addrsStructures.data()), addrsStructures.size() * sizeof(size_t));
    file.close();
}

std::filesystem::path CacheFile::GetApplicationFilePath()
{
    wchar_t szFileName[MAX_PATH + 1];
    GetModuleFileNameW(nullptr, szFileName, MAX_PATH + 1);
    return std::filesystem::path(szFileName);
}

std::filesystem::path CacheFile::GetDllFilePath()
{
    wchar_t szFileName[MAX_PATH + 1];
    GetModuleFileNameW(PyWin_DLLhModule, szFileName, MAX_PATH + 1);
    return std::filesystem::path(szFileName);
}

std::vector<uint8_t> CacheFile::CalculateSha1OfFile(const std::filesystem::path& FilePath)
{
    std::vector<uint8_t> vec(20);

    std::ifstream file(FilePath, std::ios_base::in | std::ios_base::binary);
    digestpp::sha1().absorb(file).digest(vec.data(), vec.capacity());

    return vec;
}
