/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2019 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#define Py_BUILD_CORE 1

#include <Windows.h>
#include <Python.h>

#include "Libpython.h"
#include "FixSysobject.h"
#include "FixWinapi.h"
#include "Initializer.h"

HMODULE PyWin_DLLhModule = NULL;

char* PyWin_DLLVersionString = "2.7";

BOOL WINAPI DllMain(IN HINSTANCE hDllHandle, IN DWORD nReason, IN LPVOID Reserved) 
{
    switch ( nReason ) 
    {
        case DLL_PROCESS_ATTACH:
            PyWin_DLLhModule = hDllHandle;
            break;
		case DLL_PROCESS_DETACH:
            if(!FixWinapi::Disable())
                return FALSE;
			break;
        default:
            break;
    }

    return TRUE;
}

PyMethodDef python27Methods[] = {
	{ 0, 0, 0, 0 }
};

extern"C" __declspec(dllexport) void initpython27(void)
{
	if (!Initializer::Initialize())
		return;

	if (!FixSysobject::Fix())
		return;

	if (!FixWinapi::Enable())
		return;

	Py_InitModule("python27", python27Methods);
}
