#pragma once

#include "XfwNativeExport.h"

#ifdef __cplusplus
extern "C" {
#endif

XFWNATIVE_EXPORT
size_t XFWNATIVE_GetRealAddress(const char* name);

XFWNATIVE_EXPORT
size_t XFWNATIVE_GetModuleSize(const wchar_t* name);

XFWNATIVE_EXPORT
size_t XFWNATIVE_FindFunction(size_t startpos, size_t endpos, const char* pattern, const char* mask);

#ifdef __cplusplus
}
#endif
