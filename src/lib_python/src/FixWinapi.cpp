/**
* This file is part of the XVM project.
*
* Copyright (c) 2017-2019 XVM contributors.
*
* This file is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation, version 3.
*
* This file is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <Windows.h>
#include <MinHook.h>

#include "Libpython.h"
#include "Initializer.h"
#include "FixWinapi.h"


typedef FARPROC(WINAPI *GETPROCADDRESS)(HMODULE hModule, LPCSTR lpProcName);
GETPROCADDRESS GetProcAddress_real = NULL;

FARPROC WINAPI GetProcAddress_libpython(HMODULE hModule, LPCSTR lpProcName)
{
    FARPROC ret = nullptr;
    if(hModule==PyWin_DLLhModule)
    {
        ret = (FARPROC)Initializer::GetRealAddress(lpProcName);       
        if (ret != nullptr) {
            return ret;
        }
    }

    return GetProcAddress_real(hModule, lpProcName);
}

bool FixWinapi::Enable()
{
	if (MH_Initialize() != MH_OK && MH_Initialize() != MH_ERROR_ALREADY_INITIALIZED) {
		return false;
	}

	if (MH_CreateHook(&GetProcAddress, &GetProcAddress_libpython, (LPVOID*)(&GetProcAddress_real)) != MH_OK) {
		return false;
	}

	if (MH_EnableHook(&GetProcAddress) != MH_OK) {
		return false;
	}

	return true;
}

bool FixWinapi::Disable()
{
	MH_STATUS status = MH_DisableHook(&GetProcAddress);

	if (status == MH_ERROR_NOT_INITIALIZED)
		return true;

	if (status != MH_OK) {
		return false;
	}

	if (MH_Uninitialize() != MH_OK) {
		return false;
	}

	return true;
}
