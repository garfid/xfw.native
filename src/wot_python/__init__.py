"""
This file is part of the XVM Framework project.

Copyright (c) 2017-2019 XVM Team

This file is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, version 3.

This file is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""

__ALL__ = [
    'is_native_available'
    'load_native'
    'unpack_native'
]

#python
import logging
import imp
import platform
import os
import sys

#xfw
import xfw_loader.python as loader
import xfw_vfs as vfs

#############

__native_available = False
__package_id = "com.modxvm.xfw.native"

def __get_origin_path(package_id):
    mod_path = loader.get_mod_directory_path(package_id)
    return u'%s/native_%s' % (mod_path, platform.architecture()[0])

def __get_realfs_path(package_id):
    if loader.is_mod_in_realfs(package_id):
        return __get_origin_path(package_id)

    return u'%s/%s/native_%s' % (loader.XFWLOADER_TEMPDIR, package_id, platform.architecture()[0]) 

##############

def is_native_available():
    """
    Returns True if native components are successfuly loaded
    """
    return __native_available


def load_native(package_id, library_filename, module_name):
    """
    Tries to load native module and return module object
    arguments:
      * package_id: package ID defined in xfw_package.json
      * library_filename: name of dll file with extension
      * module_name: nome of module to import
    returns:
      * module object or None on error
    """

    try:
        module_path = os.path.join(__get_realfs_path(package_id), library_filename)
        #logging.info(u"[XFW/Native] Load module %s from %s" % (module_name, module_path))
        return imp.load_dynamic(module_name, module_path.encode('mbcs'))
    except Exception:
        logging.exception("[XFW/Native] Error on loading native module '%s' from package '%s'" % (library_filename, package_id))
        return None


def unpack_native(package_id):
    """
    Unpacks native files for XFW package
    arguments:
        * package_id: package ID defined in xfw_package.json
        * force: True to unpack even if module is not loaded
    returns:
        * path to unpacked files
        * None on error
    """

    #check if package is loaded
    if not loader.is_mod_exists(package_id):
        logging.warning("[XFW/Native] [unpack_native]: mod '%s' is not exists" % package_id)
        return None

    path_origin = __get_origin_path(package_id)
    if not path_origin:
        logging.warning("[XFW/Native] [unpack_native]: failed to get origin path for package '%s'" % package_id)
        return None

    path_realfs = __get_realfs_path(package_id)
    if not path_realfs:
        logging.warning("[XFW/Native] [unpack_native]: failed to calculate realfs path for package '%s'" % package_id)
        return None

    #check paths for validity
    if loader.is_mod_in_realfs(package_id):
        if not os.path.exists(path_origin):
            logging.warning("[XFW/Native] [unpack_native]: there is no RealFS files to load for package '%s' and architecture '%s'" % (package_id, platform.architecture()[0]))
            return None
    else:
        if not vfs.directory_exists(path_origin):
            logging.warning("[XFW/Native] [unpack_native]: there is no VFS files to load for package '%s' and architecture '%s'" % (package_id, platform.architecture()[0]))
            return None

    #unpack from VFS
    vfs.directory_copy(path_origin, path_realfs)

    return path_realfs

#####

def xfw_is_module_loaded():
    return is_native_available()

def __init():
    global __native_available

    #XFW Native components loading
    try:
        #unpack libraries
        if not unpack_native(__package_id):
            logging.error("[XFW/Native] [init] Failed to unpack files")
            return

        #add to sys.path
        sys.path.insert(0, __get_realfs_path(__package_id))

        #load
        if not load_native(__package_id, 'python27.dll', 'python27'):
            logging.error("[XFW/Native] [init] Failed to load python27")
            return

        if not load_native(__package_id, 'xfw_wotexport.dll', 'xfw_wotexport'):
            logging.error("[XFW/Native] [init] Failed to load xfw_wotexport")
            return

        if not load_native(__package_id, 'xfw_d3dhook.dll', 'xfw_d3dhook'):
            logging.error("[XFW/Native] [init] Failed to load D3DHook")
            return

        __native_available = True

    except Exception:
        logging.exception("[XFW/Native] Error on loading native components. XFW Native will be unavailable.")

__init()
