/*
 * This file is part of the XFW Native project.
 *
 * Copyright (c) 2017-2019 XFW Native contributors.
 *
 * XFW Performance Monitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <Windows.h>
#include <Python.h>

#ifdef _WIN64
#include "wotexport_64bit.h"
#else
#include "wotexport_32bit.h"
#endif

static PyMethodDef xfw_wotexport_methods[] = {
    { NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC initxfw_wotexport(void)
{
    WotExport_FillAddr();
    Py_InitModule("xfw_wotexport", xfw_wotexport_methods);
}

BOOL WINAPI DllMain(
    _In_ HINSTANCE hinstDLL,
    _In_ DWORD     fdwReason,
    _In_ LPVOID    lpvReserved){
    switch (fdwReason)
    {
        case DLL_PROCESS_ATTACH:
            DisableThreadLibraryCalls(hinstDLL);
            break;
        default:
            break;
    }

    return TRUE;
}
