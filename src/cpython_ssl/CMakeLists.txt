# This file is part of the XVM project.
#
# Copyright (c) 2017-2019 XVM contributors.
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, version 3.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required (VERSION 3.6)

project(ssl LANGUAGES C)

set(OPENSSL_ROOT_DIR "${CMAKE_SOURCE_DIR}/../../depends/openssl")

find_package(libpython REQUIRED)
find_package(OpenSSL REQUIRED)


add_library(_ssl SHARED "src/_ssl.c")
target_include_directories(_ssl PRIVATE "include")
set_target_properties(_ssl PROPERTIES SUFFIX ".pyd")

target_compile_options(_ssl PRIVATE "/d2FH4-")

target_link_libraries(_ssl PRIVATE libpython::python27)
target_link_libraries(_ssl PRIVATE OpenSSL::SSL)
target_link_libraries(_ssl PRIVATE ws2_32 crypt32)

install(TARGETS _ssl 
        RUNTIME DESTINATION "bin"
)
