/*
 * This file is part of the XFW Native project.
 *
 * Copyright (c) 2017-2018 XFW Native contributors.
 *
 * XFW Performance Monitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <windows.h>
#include <d3d9.h>
#include <dxgi.h>

typedef void(__stdcall* D3D9_subscriber_typedef) (IDirect3DDevice9* pDevice);
typedef HRESULT(__stdcall *DXGI_SwapChain_Present_typedef) (IDXGISwapChain* pSwapChain, UINT SyncInterval, UINT Flags);
typedef HRESULT(__stdcall *DXGI_SwapChain_ResizeTarget_typedef) (IDXGISwapChain* pSwapChain, const DXGI_MODE_DESC *pNewTargetParameters);

BOOL D3D9_AddSubscriber(const char* id, D3D9_subscriber_typedef subscriber);
BOOL D3D9_DelSubscriber(const char* id);

BOOL DXGI_SwapChain_Present_Subscribe(const char* id, DXGI_SwapChain_Present_typedef subscriber);
BOOL DXGI_SwapChain_Present_Unsubscribe(const char* id);

BOOL DXGI_SwapChain_ResizeTarget_Subscribe(const char* id, DXGI_SwapChain_ResizeTarget_typedef subscriber);
BOOL DXGI_SwapChain_ResizeTarget_Unsubscribe(const char* id);

#ifdef __cplusplus
}
#endif