/*
 * This file is part of the XFW Performance Monitor project.
 *
 * Copyright (c) 2017 XFW Performance Monitor contributors.
 * 
 * XFW Performance Monitor is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <map>
#include <vector>
#include <string>

#include <windows.h>
#include <dxgi.h>

#include "xfw_d3dhook.h"

class DxgiHookManager
{
public:
    DxgiHookManager();
    ~DxgiHookManager();

    //DXGISwapChain::Present
    void Present_Subscribe(std::string id, DXGI_SwapChain_Present_typedef subscriber);
    void Present_Unsubscribe(std::string id);
    std::map<std::string, DXGI_SwapChain_Present_typedef> Present_GetSubscribers();

    //DXGISwapChain::ResizeTarget
    void ResizeTarget_Subscribe(std::string id, DXGI_SwapChain_ResizeTarget_typedef subscriber);
    void ResizeTarget_Unsubscribe(std::string id);
    std::map<std::string, DXGI_SwapChain_ResizeTarget_typedef> ResizeTarget_GetSubscribers();

private:
    void GetVtableAddress();

    std::map<std::string, DXGI_SwapChain_Present_typedef> _present_subscribers;
    std::map<std::string, DXGI_SwapChain_ResizeTarget_typedef> _resizeTarget_subscribers;

    //DXGISwapChain
    IDXGISwapChain* _pSwapChain = nullptr;
    DWORD* _VTableDXGISwapChain = nullptr;

    //DXGISwapChain::Present
    bool Present_Hook();
    bool Present_Unhook();
    static HRESULT __stdcall DXGISwapChain_Present_detour(IDXGISwapChain* pSwapChain, UINT SyncInterval, UINT Flags);
    DXGI_SwapChain_Present_typedef DXGISwapChain_Present_trampoline = nullptr;

    //DXGISwapChain::ResizeTarget
    bool ResizeTarget_Hook();
    bool ResizeTarget_Unhook();
    static HRESULT __stdcall DXGISwapChain_ResizeTarget_detour(IDXGISwapChain* pSwapChain, const DXGI_MODE_DESC *pNewTargetParameters);
    DXGI_SwapChain_ResizeTarget_typedef DXGISwapChain_ResizeTarget_trampoline = nullptr;
};
