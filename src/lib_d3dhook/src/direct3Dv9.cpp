/*
 * This file is part of the XFW Performance Monitor project.
 *
 * Copyright (c) 2017 XFW Performance Monitor contributors.
 * 
 * XFW Performance Monitor is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "direct3Dv9.h"
#include "xfw_d3dhook_internal.h"
#include "MinHook.h"


D3D9HookManager::D3D9HookManager()
{
}

D3D9HookManager::~D3D9HookManager()
{
    Unhook();
}

//Direct3DDevice9::EndScene
HRESULT WINAPI D3D9HookManager::Direct3DDevice9_EndScene_detour(IDirect3DDevice9* pDevice)
{
    for (auto& subscriber : hookmgr_d3d9->GetSubscribers())
    {
        if(subscriber.second != nullptr)
            subscriber.second(pDevice);
    }
    return hookmgr_d3d9->Direct3DDevice9_EndScene_trampoline(pDevice);
}

bool D3D9HookManager::Hook()
{
    if(MH_CreateHook(reinterpret_cast<void *>(D3D9HookManager::_VTableIDirect3DDevice9[42]), &Direct3DDevice9_EndScene_detour, reinterpret_cast<void**>(&Direct3DDevice9_EndScene_trampoline)) !=MH_OK)
        return false;

    if (MH_EnableHook(reinterpret_cast<void *>(D3D9HookManager::_VTableIDirect3DDevice9[42])) != MH_OK)
        return false;

    return true;
}

bool D3D9HookManager::Unhook()
{
    if (_VTableIDirect3DDevice9 == nullptr)
        return false;

    if (MH_DisableHook(reinterpret_cast<void *>(D3D9HookManager::_VTableIDirect3DDevice9[42])) != MH_OK)
        return false;

    if (MH_RemoveHook(reinterpret_cast<void *>(D3D9HookManager::_VTableIDirect3DDevice9[42])) != MH_OK)
        return false;

    return true;
}


void D3D9HookManager::GetVtableAddress()
{
    //D3D9
    IDirect3DDevice9* pD3D9Device = nullptr;
    IDirect3D9* pD3D9c = Direct3DCreate9(D3D_SDK_VERSION);
    if (pD3D9c == nullptr)
        return;

    //set display mode
    D3DDISPLAYMODE d3d_displaymode;
    if (pD3D9c->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3d_displaymode) != D3D_OK)
        return;

    //set present parameters
    D3DPRESENT_PARAMETERS d3d_presentparamters;
    ZeroMemory(&d3d_presentparamters, sizeof(d3d_presentparamters));
    d3d_presentparamters.Windowed = true;
    d3d_presentparamters.SwapEffect = D3DSWAPEFFECT_DISCARD;
    d3d_presentparamters.BackBufferFormat = d3d_displaymode.Format;

    //create window
    WNDCLASSEX wc = { sizeof(WNDCLASSEX),CS_CLASSDC,DefWindowProc,0L,0L,GetModuleHandleA(NULL),NULL,NULL,NULL,NULL,"1",NULL };
    RegisterClassExA(&wc);
    HWND hWnd = CreateWindowExA(0, "1", NULL, WS_OVERLAPPEDWINDOW, 100, 100, 300, 300, GetDesktopWindow(), NULL, wc.hInstance, NULL);
    if (hWnd == nullptr)
        return;

    //create device
    if (pD3D9c->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_DISABLE_DRIVER_MANAGEMENT, &d3d_presentparamters, &pD3D9Device) != D3D_OK)
        return;

    //get VTable
    _VTableIDirect3DDevice9 = reinterpret_cast<DWORD*>(pD3D9Device);
    _VTableIDirect3DDevice9 = reinterpret_cast<DWORD*>(_VTableIDirect3DDevice9[0]);

    //free resources
    pD3D9c->Release();
    DestroyWindow(hWnd);
}

void D3D9HookManager::AddSubscriber(std::string id, D3D9_subscriber_typedef subscriber)
{
    if (_subscribers.empty())
    {
        if (_VTableIDirect3DDevice9 == nullptr)
            GetVtableAddress();

        Hook();
    }

    _subscribers.emplace(id, subscriber);
}

void D3D9HookManager::DelSubscriber(std::string id)
{
    _subscribers.erase(id);
}

std::map<std::string, D3D9_subscriber_typedef> D3D9HookManager::GetSubscribers()
{
    return _subscribers;
}
