/*
 * This file is part of the XFW Native project.
 *
 * Copyright (c) 2017-2018 XFW Native contributors.
 * 
 * XFW Performance Monitor is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <map>
#include <vector>
#include <string>

#include <windows.h>
#include <d3d9.h>

#include "xfw_d3dhook.h"

class D3D9HookManager
{
public:
    D3D9HookManager();
    ~D3D9HookManager();
    
    void AddSubscriber(std::string id, D3D9_subscriber_typedef subscriber);
    void DelSubscriber(std::string id);
    std::map<std::string,D3D9_subscriber_typedef> GetSubscribers();

private:
    bool Hook();
    bool Unhook();

    void GetVtableAddress();

    std::map<std::string, D3D9_subscriber_typedef> _subscribers;

    //IDirect3DDevice9
    DWORD* _VTableIDirect3DDevice9 = nullptr;
    
    //IDirect3DDevice9::EndScene
    typedef HRESULT(WINAPI* Direct3DDevice9_EndScene_typedef)(LPDIRECT3DDEVICE9 pDevice);
    Direct3DDevice9_EndScene_typedef Direct3DDevice9_EndScene_trampoline = nullptr;
    static HRESULT WINAPI Direct3DDevice9_EndScene_detour(LPDIRECT3DDEVICE9 pDevice);
};
