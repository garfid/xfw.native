/*
 * This file is part of the XFW Performance Monitor project.
 *
 * Copyright (c) 2017 XFW Performance Monitor contributors.
 * 
 * XFW Performance Monitor is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU Lesser General Public License as   
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "direct3Dv11.h"
#include "xfw_d3dhook_internal.h"

#include "MinHook.h"

#include <d3d11.h>

DxgiHookManager::DxgiHookManager()
{
}

DxgiHookManager::~DxgiHookManager()
{
    Present_Unhook();
    ResizeTarget_Unhook();
}

void DxgiHookManager::GetVtableAddress()
{
    HWND hWnd = GetForegroundWindow();

    ID3D11Device *pDevice = nullptr;
    ID3D11DeviceContext *pContext = nullptr;
    
    D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;
    DXGI_SWAP_CHAIN_DESC swapChainDesc;
    ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
    swapChainDesc.BufferCount = 1;
    swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapChainDesc.OutputWindow = hWnd;
    swapChainDesc.SampleDesc.Count = 1;
    swapChainDesc.Windowed = ((GetWindowLong(hWnd, GWL_STYLE) & WS_POPUP) != 0) ? FALSE : TRUE;
    swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

    if (FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, NULL, &featureLevel, 1, D3D11_SDK_VERSION, &swapChainDesc, &_pSwapChain, &pDevice, NULL, &pContext)))
        return;

    _VTableDXGISwapChain = reinterpret_cast<DWORD*>(_pSwapChain);
    _VTableDXGISwapChain = reinterpret_cast<DWORD*>(_VTableDXGISwapChain[0]);

    pDevice->Release();
    pContext->Release();
}


//DXGISwapChain::Present
void DxgiHookManager::Present_Subscribe(std::string id, DXGI_SwapChain_Present_typedef subscriber)
{
    if (_present_subscribers.empty())
    {
        if (_VTableDXGISwapChain == nullptr)
            GetVtableAddress();

        Present_Hook();
    }

    _present_subscribers.emplace(id, subscriber);
}

void DxgiHookManager::Present_Unsubscribe(std::string id)
{
    _present_subscribers.erase(id);
}

std::map<std::string, DXGI_SwapChain_Present_typedef> DxgiHookManager::Present_GetSubscribers()
{
    return _present_subscribers;
}

bool DxgiHookManager::Present_Hook() {
    if (MH_CreateHook(reinterpret_cast<void *>(_VTableDXGISwapChain[8]), &DxgiHookManager::DXGISwapChain_Present_detour, reinterpret_cast<void**>(&DXGISwapChain_Present_trampoline)) != MH_OK)
        return false;

    if (MH_EnableHook(reinterpret_cast<void *>(_VTableDXGISwapChain[8])) != MH_OK)
        return false;

    return true;
}

bool DxgiHookManager::Present_Unhook() {
    if (_VTableDXGISwapChain == nullptr)
        return false;

    if (MH_DisableHook(reinterpret_cast<void *>(_VTableDXGISwapChain[8])) != MH_OK)
        return false;

    if (MH_RemoveHook(reinterpret_cast<void *>(_VTableDXGISwapChain[8])) != MH_OK)
        return false;

    return true;
}

HRESULT __stdcall DxgiHookManager::DXGISwapChain_Present_detour(IDXGISwapChain* pSwapChain, UINT SyncInterval, UINT Flags)
{
    for (auto& subscriber : hookmgr_dxgi->Present_GetSubscribers())
    {
        if (subscriber.second != nullptr)
            subscriber.second(pSwapChain, SyncInterval, Flags);
    }

    return hookmgr_dxgi->DXGISwapChain_Present_trampoline(pSwapChain, SyncInterval, Flags);
}


//DXGISwapChain::ResizeTarget
void DxgiHookManager::ResizeTarget_Subscribe(std::string id, DXGI_SwapChain_ResizeTarget_typedef subscriber)
{
    if (_resizeTarget_subscribers.empty())
    {
        if (_VTableDXGISwapChain == nullptr)
            GetVtableAddress();

        ResizeTarget_Hook();
    }

    _resizeTarget_subscribers.emplace(id, subscriber);
}

void DxgiHookManager::ResizeTarget_Unsubscribe(std::string id)
{
    _resizeTarget_subscribers.erase(id);
}

std::map<std::string, DXGI_SwapChain_ResizeTarget_typedef> DxgiHookManager::ResizeTarget_GetSubscribers()
{
    return _resizeTarget_subscribers;
}

bool DxgiHookManager::ResizeTarget_Hook() {
    if (MH_CreateHook(reinterpret_cast<void *>(_VTableDXGISwapChain[14]), &DxgiHookManager::DXGISwapChain_ResizeTarget_detour, reinterpret_cast<void**>(&DXGISwapChain_ResizeTarget_trampoline)) != MH_OK)
        return false;

    if (MH_EnableHook(reinterpret_cast<void *>(_VTableDXGISwapChain[14])) != MH_OK)
        return false;

    return true;
}

bool DxgiHookManager::ResizeTarget_Unhook() {
    if (_VTableDXGISwapChain == nullptr)
        return false;

    if (MH_DisableHook(reinterpret_cast<void *>(_VTableDXGISwapChain[14])) != MH_OK)
        return false;

    if (MH_RemoveHook(reinterpret_cast<void *>(_VTableDXGISwapChain[14])) != MH_OK)
        return false;

    return true;
}

HRESULT __stdcall DxgiHookManager::DXGISwapChain_ResizeTarget_detour(IDXGISwapChain* pSwapChain, const DXGI_MODE_DESC *pNewTargetParameters)
{
    for (auto& subscriber : hookmgr_dxgi->ResizeTarget_GetSubscribers())
    {
        if (subscriber.second != nullptr)
            subscriber.second(pSwapChain, pNewTargetParameters);
    }

    return hookmgr_dxgi->DXGISwapChain_ResizeTarget_trampoline(pSwapChain, pNewTargetParameters);
}

