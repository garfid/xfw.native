/*
 * This file is part of the XFW Native project.
 *
 * Copyright (c) 2017-2018 XFW Native contributors.
 *
 * XFW Performance Monitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "xfw_d3dhook.h"
#include "xfw_d3dhook_internal.h"

#include "direct3Dv9.h"
#include "direct3Dv11.h"

#include <Python.h>

D3D9HookManager* hookmgr_d3d9 = nullptr;
DxgiHookManager* hookmgr_dxgi = nullptr;

static PyMethodDef xfw_d3dhook_methods[] = {
    { nullptr, nullptr, 0, nullptr} 
};

PyMODINIT_FUNC initxfw_d3dhook(void)
{
    Py_InitModule("xfw_d3dhook", xfw_d3dhook_methods);
}

BOOL WINAPI DllMain(
    _In_ HINSTANCE hinstDLL,
    _In_ DWORD     fdwReason,
    _In_ LPVOID    lpvReserved
)
{
    switch (fdwReason)
    {
    case DLL_PROCESS_ATTACH:
        DisableThreadLibraryCalls(hinstDLL);
        hookmgr_d3d9 = new D3D9HookManager();
        hookmgr_dxgi = new DxgiHookManager();
        break;
    case DLL_PROCESS_DETACH:
        if (hookmgr_d3d9 != nullptr)
        {
            delete hookmgr_d3d9;
            hookmgr_d3d9 = nullptr;
        }

        if (hookmgr_dxgi != nullptr)
        {
            delete hookmgr_dxgi;
            hookmgr_dxgi = nullptr;
        }
        break;

    default:
        break;
    }
    return TRUE;
}

BOOL D3D9_AddSubscriber(const char* id, D3D9_subscriber_typedef subscriber)
{
    if (hookmgr_d3d9 == nullptr)
        return FALSE;

    hookmgr_d3d9->AddSubscriber(id, subscriber);

    return TRUE;
}

BOOL D3D9_DelSubscriber(const char* id)
{
    if (hookmgr_d3d9 == nullptr)
        return FALSE;

    hookmgr_d3d9->DelSubscriber(id);

    return TRUE;
}


BOOL DXGI_SwapChain_Present_Subscribe(const char* id, DXGI_SwapChain_Present_typedef subscriber)
{
    if (hookmgr_dxgi == nullptr)
        return FALSE;

    hookmgr_dxgi->Present_Subscribe(id, subscriber);

    return TRUE;
}

BOOL DXGI_SwapChain_Present_Unsubscribe(const char* id)
{
    if (hookmgr_dxgi == nullptr)
        return FALSE;

    hookmgr_dxgi->Present_Unsubscribe(id);

    return TRUE;
}

BOOL DXGI_SwapChain_ResizeTarget_Subscribe(const char* id, DXGI_SwapChain_ResizeTarget_typedef subscriber)
{
    if (hookmgr_dxgi == nullptr)
        return FALSE;

    hookmgr_dxgi->ResizeTarget_Subscribe(id, subscriber);

    return TRUE;
}

BOOL DXGI_SwapChain_ResizeTarget_Unsubscribe(const char* id)
{
    if (hookmgr_dxgi == nullptr)
        return FALSE;

    hookmgr_dxgi->ResizeTarget_Unsubscribe(id);

    return TRUE;
}
