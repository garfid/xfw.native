/*
 * This file is part of the XFW Native project.
 *
 * Copyright (c) 2017-2018 XFW Native contributors.
 *
 * XFW Performance Monitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XFW Performance Monitor is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "direct3Dv9.h"
#include "direct3Dv11.h"

extern D3D9HookManager* hookmgr_d3d9;
extern DxgiHookManager* hookmgr_dxgi;
