# Changelog

## v 1.5.6
* audiokinetic: provide only headers
* wotexport: first version

## v 1.5.5
* python: fixed compatibility with non-latin characters in path

## v 1.5.4
* build: disable FrameHandler4
* libpython: switch from PPL to Yuki Koyama's parallel util

## v 1.5.3
* libpython: switch from OpenMP to MS PPL for concurrency 

## v 1.5.2
* libpython: add additional 64-bit signatures

## v 1.5.1
* libpython: initial 64-bit support

## v. 1.5.0
* libpython: implemented parallel signature search via OpenMP
* libpython: preparation for 64-bit

## v. 1.4.9
* python: fix `unpack_native` behaviour
* python: add `xfw_is_module_loaded` function
* python: fix false `__native_available` flag value on error

## v. 1.4.8
* adopt to WoT 1.6.1.0

## v. 1.4.7
* updated meta

## v. 1.4.6
* move repository to GitLab

## v. 1.4.5
* libpython: finished PyCapsule API
* libpython: added PyUnicodeUCS2_DecodeUTF[16|32]{Stateful}
* pybind11: added pybind11 v2.3.0
* sqlite: fixed module initialization

## v. 1.4.4
* audiokinetic: fixed module initialization
* ida: removed dependency from simplejson
* libpython: fixed cache file deserialization
* libpython: add offsets for WoT 1.6.0.365
* libpython: fixed crash in `PyUnicodeUCS_FromFormat`
* libpython: updated `object.h` and `intobject.h` headers to CPython 2.7.16
* sqlite: add `sqlite3` module backport from CPython 2.7.16

## v. 1.4.3

* libpython: added `PyUnicodeUCS_FromFormat` and `PyUnicodeUCS_FromFormatV`
* libpython: fixed structures array size
* libaudiokinetic: added SoundEngine component registration functions
* build: fixed compilation for CMake 3.14 and MSVS 2019

## v. 1.4.2

* libpython: fixed compatibility with WoT 1.4.0.227
* tool_ida: added ability to import types to IDA for non-WoT binaries

## v. 1.4.1

* libpython: fixed compatibility with WoT 1.3.0.126
* libpython: improved debug output


## v. 1.4.0

* libpython: fixed compatibility with WoT 1.2.0.58
* libpython: new JSON file format
* libpython: added support for multiple signatures for one structure
* libpython: fixed multiple matching validation


## v. 1.3.1

* xfw_d3dhook: added ability to hook IDXGISwapChain:ResizeTarget
* xfw_d3dhook: rename D3D11 to DXGI

## v. 1.3.0

* xfw_d3dhook: added library for D3D9/DXGI hooking
* xfw_package: added `features_provide` section
* libpython: updated offsets
* libpython: added `extern "C"` in `PythonWoT.h` header 
* libpython: implemented PyCode::Type
* libpython: implemented PyFrozenSet
* libpython: implemented PyFunction
* libpython: implemented PyNumber
* libpython: implemented PyInstance
* libpython: implemented PyObject
* libpython: implemented PySet
* libpython: implemented PyUnicodeUCS::Compare

## v. 1.2.0

* implemented libpython signatures cache
* implemented structures runtime validation 

## v. 1.1.1

* World of Tanks 1.0.1 Micropatch 1

## v. 1.1.0

* World of Tanks 1.0.1 Common Test 2
* Unpack native modules to game client directory instead of user temporary directory due to World of Tanks bug

## v. 1.0.9

* World of Tanks 1.0.0 micropatch 1

## v. 1.0.8

* World of Tanks 1.0.0 Common Test 6
* Improve debug output
* Improve runtime signature matching

## v. 1.0.7

* fix null pointer derefencing on dynamic minhook loading

## v. 1.0.6

* compatible with WoT 9.22 CT

## v. 1.0.5

* audiokinetic: added `AK:Comm` namespace
* libpython: fix some data structures
* libpython: implement some more types structures

##  v. 1.0.4

* export some helper functions

## v. 1.0.3

* provide signed binaries
* add some new signatures

## v. 1.0.2

* compatible with WoT 9.21.0
* update xfw_packages.json to latest specification


## v. 1.0.1

* change **.wotmod** and **.zip** layout


## v. 1.0.0

* first public version
* compatible with WoT 9.20.1
