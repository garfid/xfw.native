typedef int Py_ssize_t;

typedef unsigned short digit;

typedef wchar_t Py_UNICODE;

typedef long long PY_LONG_LONG;

typedef unsigned long long U_PY_LONG_LONG;

struct _object
{
  int ob_refcnt;
};

struct bufferinfo
{
  void *buf;
  _object *obj;
  int len;
  int itemsize;
  int readonly;
  int ndim;
  char *format;
  int *shape;
  int *strides;
  int *suboffsets;
  int smalltable[2];
  void *internal;
};

struct PyMemberDef
{
  char *name;
  int type;
  int offset;
  int flags;
  char *doc;
};

struct PyGetSetDef
{
  char *name;
  _object *(__cdecl *get)(_object *, void *);
  int (__cdecl *set)(_object *, _object *, void *);
  char *doc;
  void *closure;
};

struct PyNumberMethods
{
  _object *(__cdecl *nb_add)(_object *, _object *);
  _object *(__cdecl *nb_subtract)(_object *, _object *);
  _object *(__cdecl *nb_multiply)(_object *, _object *);
  _object *(__cdecl *nb_divide)(_object *, _object *);
  _object *(__cdecl *nb_remainder)(_object *, _object *);
  _object *(__cdecl *nb_divmod)(_object *, _object *);
  _object *(__cdecl *nb_power)(_object *, _object *, _object *);
  _object *(__cdecl *nb_negative)(_object *);
  _object *(__cdecl *nb_positive)(_object *);
  _object *(__cdecl *nb_absolute)(_object *);
  int (__cdecl *nb_nonzero)(_object *);
  _object *(__cdecl *nb_invert)(_object *);
  _object *(__cdecl *nb_lshift)(_object *, _object *);
  _object *(__cdecl *nb_rshift)(_object *, _object *);
  _object *(__cdecl *nb_and)(_object *, _object *);
  _object *(__cdecl *nb_xor)(_object *, _object *);
  _object *(__cdecl *nb_or)(_object *, _object *);
  int (__cdecl *nb_coerce)(_object **, _object **);
  _object *(__cdecl *nb_int)(_object *);
  _object *(__cdecl *nb_long)(_object *);
  _object *(__cdecl *nb_float)(_object *);
  _object *(__cdecl *nb_oct)(_object *);
  _object *(__cdecl *nb_hex)(_object *);
  _object *(__cdecl *nb_inplace_add)(_object *, _object *);
  _object *(__cdecl *nb_inplace_subtract)(_object *, _object *);
  _object *(__cdecl *nb_inplace_multiply)(_object *, _object *);
  _object *(__cdecl *nb_inplace_divide)(_object *, _object *);
  _object *(__cdecl *nb_inplace_remainder)(_object *, _object *);
  _object *(__cdecl *nb_inplace_power)(_object *, _object *, _object *);
  _object *(__cdecl *nb_inplace_lshift)(_object *, _object *);
  _object *(__cdecl *nb_inplace_rshift)(_object *, _object *);
  _object *(__cdecl *nb_inplace_and)(_object *, _object *);
  _object *(__cdecl *nb_inplace_xor)(_object *, _object *);
  _object *(__cdecl *nb_inplace_or)(_object *, _object *);
  _object *(__cdecl *nb_floor_divide)(_object *, _object *);
  _object *(__cdecl *nb_true_divide)(_object *, _object *);
  _object *(__cdecl *nb_inplace_floor_divide)(_object *, _object *);
  _object *(__cdecl *nb_inplace_true_divide)(_object *, _object *);
  _object *(__cdecl *nb_index)(_object *);
};

struct PySequenceMethods
{
  int (__cdecl *sq_length)(_object *);
  _object *(__cdecl *sq_concat)(_object *, _object *);
  _object *(__cdecl *sq_repeat)(_object *, int);
  _object *(__cdecl *sq_item)(_object *, int);
  _object *(__cdecl *sq_slice)(_object *, int, int);
  int (__cdecl *sq_ass_item)(_object *, int, _object *);
  int (__cdecl *sq_ass_slice)(_object *, int, int, _object *);
  int (__cdecl *sq_contains)(_object *, _object *);
  _object *(__cdecl *sq_inplace_concat)(_object *, _object *);
  _object *(__cdecl *sq_inplace_repeat)(_object *, int);
};

struct PyMappingMethods
{
  int (__cdecl *mp_length)(_object *);
  _object *(__cdecl *mp_subscript)(_object *, _object *);
  int (__cdecl *mp_ass_subscript)(_object *, _object *, _object *);
};

struct PyBufferProcs
{
  int (__cdecl *bf_getreadbuffer)(_object *, int, void **);
  int (__cdecl *bf_getwritebuffer)(_object *, int, void **);
  int (__cdecl *bf_getsegcount)(_object *, int *);
  int (__cdecl *bf_getcharbuffer)(_object *, int, char **);
  int (__cdecl *bf_getbuffer)(_object *, bufferinfo *, int);
  void (__cdecl *bf_releasebuffer)(_object *, bufferinfo *);
};

struct PyMethodDef
{
  const char *ml_name;
  _object *(__cdecl *ml_meth)(_object *, _object *);
  int ml_flags;
  const char *ml_doc;
};

struct _typeobject
{
  int ob_refcnt;
};

struct _typeobject
{
  int ob_refcnt;
  _typeobject *ob_type;
  int ob_size;
  const char *tp_name;
  int tp_basicsize;
  int tp_itemsize;
  void (__cdecl *tp_dealloc)(_object *);
  int (__cdecl *tp_print)(_object *, _iobuf *, int);
  _object *(__cdecl *tp_getattr)(_object *, char *);
  int (__cdecl *tp_setattr)(_object *, char *, _object *);
  int (__cdecl *tp_compare)(_object *, _object *);
  _object *(__cdecl *tp_repr)(_object *);
  PyNumberMethods *tp_as_number;
  PySequenceMethods *tp_as_sequence;
  PyMappingMethods *tp_as_mapping;
  int (__cdecl *tp_hash)(_object *);
  _object *(__cdecl *tp_call)(_object *, _object *, _object *);
  _object *(__cdecl *tp_str)(_object *);
  _object *(__cdecl *tp_getattro)(_object *, _object *);
  int (__cdecl *tp_setattro)(_object *, _object *, _object *);
  PyBufferProcs *tp_as_buffer;
  int tp_flags;
  const char *tp_doc;
  int (__cdecl *tp_traverse)(_object *, int (__cdecl *)(_object *, void *), void *);
  int (__cdecl *tp_clear)(_object *);
  _object *(__cdecl *tp_richcompare)(_object *, _object *, int);
  int tp_weaklistoffset;
  _object *(__cdecl *tp_iter)(_object *);
  _object *(__cdecl *tp_iternext)(_object *);
  PyMethodDef *tp_methods;
  PyMemberDef *tp_members;
  PyGetSetDef *tp_getset;
  _typeobject *tp_base;
  _object *tp_dict;
  _object *(__cdecl *tp_descr_get)(_object *, _object *, _object *);
  int (__cdecl *tp_descr_set)(_object *, _object *, _object *);
  int tp_dictoffset;
  int (__cdecl *tp_init)(_object *, _object *, _object *);
  _object *(__cdecl *tp_alloc)(_typeobject *, int);
  _object *(__cdecl *tp_new)(_typeobject *, _object *, _object *);
  void (__cdecl *tp_free)(void *);
  int (__cdecl *tp_is_gc)(_object *);
  _object *tp_bases;
  _object *tp_mro;
  _object *tp_cache;
  _object *tp_subclasses;
  _object *tp_weaklist;
  void (__cdecl *tp_del)(_object *);
  unsigned int tp_version_tag;
};

typedef _typeobject PyTypeObject;

struct _object
{
  int ob_refcnt;
  _typeobject *ob_type;
};

typedef _object PyObject;

struct _block 
{
    size_t ab_size;
};

struct _block 
{
    size_t ab_size;
    size_t ab_offset;
    struct _block *ab_next;
    void *ab_mem;
};

typedef _block block;

struct _arena 
{
    block *a_head;
    block *a_cur;
    PyObject *a_objects;
};

typedef _arena PyArena;

struct PyCompilerFlags
{
    int cf_flags;
};

struct bufferinfo 
{
    void *buf;
    PyObject *obj;
    Py_ssize_t len;
    Py_ssize_t itemsize;
    int readonly;
    int ndim;
    char *format;
    Py_ssize_t *shape;
    Py_ssize_t *strides;
    Py_ssize_t *suboffsets;
    Py_ssize_t smalltable[2];
    void *internal;
};

typedef bufferinfo Py_buffer;

struct PyCodeObject
{
    Py_ssize_t ob_refcnt;
    struct _typeobject *ob_type;
    int co_argcount;
    int co_nlocals;
    int co_stacksize;
    int co_flags;
    PyObject *co_code;
    PyObject *co_consts;
    PyObject *co_names;
    PyObject *co_varnames;
    PyObject *co_freevars;
    PyObject *co_cellvars;
    PyObject *co_filename;
    PyObject *co_name;
    int co_firstlineno;
    PyObject *co_lnotab;
    void *co_zombieframe;
    PyObject *co_weakreflist;
};

struct _is
{
    PyObject *modules;
};

typedef _is PyInterpreterState;

struct _ts
{
    PyInterpreterState *interp;
};

typedef _ts PyThreadState;

struct _is
{
    struct _is *next;
    struct _ts *tstate_head;
    PyObject *modules;
    PyObject *sysdict;
    PyObject *builtins;
    PyObject *modules_reloading;
    PyObject *codec_search_path;
    PyObject *codec_search_cache;
    PyObject *codec_error_registry;
};

struct PyTryBlock
{
    int b_type;
    int b_handler;
    int b_level;
};

struct _frame
{
    PyInterpreterState *interp;
};

struct _frame 
{
    Py_ssize_t ob_refcnt;
    struct _typeobject *ob_type;
    Py_ssize_t ob_size;
    struct _frame *f_back;
    PyCodeObject *f_code;
    PyObject *f_builtins;
    PyObject *f_globals;
    PyObject *f_locals;
    PyObject **f_valuestack;
    PyObject **f_stacktop;
    PyObject *f_trace;
    PyObject *f_exc_type, *f_exc_value, *f_exc_traceback;
    PyThreadState *f_tstate;
    int f_lasti;
    int f_lineno;
    int f_iblock;
    PyTryBlock f_blockstack[20];
    PyObject *f_localsplus[1];
};

typedef _frame PyFrameObject;

typedef int (*Py_tracefunc)(PyObject *, struct _frame *, int, PyObject *);

struct _ts 
{
    struct _ts *next;
    PyInterpreterState *interp;
    struct _frame *frame;
    int recursion_depth;
    int tracing;
    int use_tracing;
    Py_tracefunc c_profilefunc;
    Py_tracefunc c_tracefunc;
    PyObject *c_profileobj;
    PyObject *c_traceobj;
    PyObject *curexc_type;
    PyObject *curexc_value;
    PyObject *curexc_traceback;
    PyObject *exc_type;
    PyObject *exc_value;
    PyObject *exc_traceback;
    PyObject *dict;
    int tick_counter;
    int gilstate_counter;
    PyObject *async_exc;
    long thread_id;
    int trash_delete_nesting;
    PyObject *trash_delete_later;
};

typedef int (__stdcall *dl_funcptr)();

typedef void *PyThread_type_lock;

typedef void *PyThread_type_sema;

struct PyVarObject
{
    Py_ssize_t ob_refcnt;
    struct _typeobject *ob_type;
    Py_ssize_t ob_size;
};

typedef PyObject *(*PyCFunction)(PyObject *, PyObject *);

typedef void (*PyCapsule_Destructor)(PyObject *);

struct _addr_pair
{
    int ap_lower;
    int ap_upper;
};

typedef _addr_pair PyAddrPair;

struct _frozen 
{
    char *name;
    unsigned char *code;
    int size;
};

struct PyGenObject
{
    Py_ssize_t ob_refcnt;
    struct _typeobject *ob_type;
    struct _frame *gi_frame;
    int gi_running;
    PyObject *gi_code;
    PyObject *gi_weakreflist;
};

struct asdl_seq
{
    int size;
    void *elements[1];
};

struct asdl_int_seq
{
    int size;
    int elements[1];
};

typedef PyObject * identifier;

typedef PyObject * string;

typedef PyObject * object;

struct _arguments 
{
    asdl_seq *args;
    identifier vararg;
    identifier kwarg;
    asdl_seq *defaults;
};

typedef _arguments *arguments_ty;

struct _expr 
{
    _expr_kind kind;
};

typedef _expr *expr_ty;

struct _slice {
    _slice_kind kind;
    union {
        struct {
            expr_ty lower;
            expr_ty upper;
            expr_ty step;
        } Slice;   
        struct {
            asdl_seq *dims;
        } ExtSlice;  
        struct {
            expr_ty value;
        } Index;      
    } v;
};

typedef _slice *slice_ty;

struct _expr {
    _expr_kind kind;
    union {
        struct {
            _boolop op;
            asdl_seq *values;
        } BoolOp;             
        struct {
            expr_ty left;
            _operator op;
            expr_ty right;
        } BinOp;
        struct {
            _unaryop op;
            expr_ty operand;
        } UnaryOp;
        struct {
            arguments_ty args;
            expr_ty body;
        } Lambda;
        struct {
            expr_ty test;
            expr_ty body;
            expr_ty orelse;
        } IfExp;
        struct {
            asdl_seq *keys;
            asdl_seq *values;
        } Dict;
        struct {
            asdl_seq *elts;
        } Set;
        struct {
            expr_ty elt;
            asdl_seq *generators;
        } ListComp;
        struct {
            expr_ty elt;
            asdl_seq *generators;
        } SetComp;
        struct {
            expr_ty key;
            expr_ty value;
            asdl_seq *generators;
        } DictComp;  
        struct {
            expr_ty elt;
            asdl_seq *generators;
        } GeneratorExp;   
        struct {
            expr_ty value;
        } Yield;       
        struct {
            expr_ty left;
            asdl_int_seq *ops;
            asdl_seq *comparators;
        } Compare;          
        struct {
            expr_ty func;
            asdl_seq *args;
            asdl_seq *keywords;
            expr_ty starargs;
            expr_ty kwargs;
        } Call;           
        struct {
            expr_ty value;
        } Repr;             
        struct {
            object n;
        } Num;       
        struct {
            string s;
        } Str;       
        struct {
            expr_ty value;
            identifier attr;
            _expr_context ctx;
        } Attribute;          
        struct {
            expr_ty value;
            slice_ty slice;
            _expr_context ctx;
        } Subscript;              
        struct {
            identifier id;
            _expr_context ctx;
        } Name;              
        struct {
            asdl_seq *elts;
            _expr_context ctx;
        } List;              
        struct {
            asdl_seq *elts;
            _expr_context ctx;
        } Tuple;
    } v;
    int lineno;
    int col_offset;
};

struct _stmt {
    _stmt_kind kind;
    union {
        struct {
            identifier name;
            arguments_ty args;
            asdl_seq *body;
            asdl_seq *decorator_list;
        } FunctionDef;      
        struct {
            identifier name;
            asdl_seq *bases;
            asdl_seq *body;
            asdl_seq *decorator_list;
        } ClassDef;             
        struct {
                expr_ty value;
        } Return;             
        struct {
            asdl_seq *targets;
        } Delete;             
        struct {
            asdl_seq *targets;
            expr_ty value;
        } Assign;
        struct {
            expr_ty target;
            _operator op;
            expr_ty value;
        } AugAssign;
        struct {
            expr_ty dest;
            asdl_seq *values;
            bool nl;
        } Print;
        struct {
            expr_ty target;
            expr_ty iter;
            asdl_seq *body;
            asdl_seq *orelse;
        } For;
        struct {
            expr_ty test;
            asdl_seq *body;
            asdl_seq *orelse;
        } While;
        struct {
            expr_ty test;
            asdl_seq *body;
            asdl_seq *orelse;
        } If;
        struct {
            expr_ty context_expr;
            expr_ty optional_vars;
            asdl_seq *body;
        } With;
        struct {
            expr_ty type;
            expr_ty inst;
            expr_ty tback;
        } Raise;
        struct {
            asdl_seq *body;
            asdl_seq *handlers;
            asdl_seq *orelse;
        } TryExcept;
        struct {
            asdl_seq *body;
            asdl_seq *finalbody;
        } TryFinally;
        struct {
            expr_ty test;
            expr_ty msg;
        } Assert;
        struct {
            asdl_seq *names;
        } Import;
        struct {
            identifier module;
            asdl_seq *names;
            int level;
        } ImportFrom;
        struct {
            expr_ty body;
            expr_ty globals;
            expr_ty locals;
        } Exec;
        struct {
            asdl_seq *names;
        } Global;
        struct {
            expr_ty value;
        } Expr;               
    } v;
    int lineno;
    int col_offset;
};

typedef _stmt *stmt_ty;

struct _mod {
    enum _mod_kind kind;
    union {
        struct {
            asdl_seq *body;
        } Module;            
        struct {
            asdl_seq *body;
        } Interactive;         
        struct {
            expr_ty body;
        } Expression;          
        struct {
            asdl_seq *body;
        } Suite;          
    } v;
};

typedef _mod *mod_ty;

struct _node
{
    short		n_type;
};

struct _node 
{
    short		n_type;
    char		*n_str;
    int			n_lineno;
    int			n_col_offset;
    int			n_nchildren;
    struct _node	*n_child;
};

typedef _node node;

struct PyMethodChain 
{
    PyMethodDef *methods;
};

struct PyMethodChain 
{
    PyMethodDef *methods;
    PyMethodChain *link;
};

struct PyFutureFeatures
{
    int ff_features;
    int ff_lineno;
};

struct PyIntObject
{
    Py_ssize_t ob_refcnt;
    _typeobject *ob_type;
    long ob_ival;
};

struct PyListObject
{    
    Py_ssize_t ob_refcnt;
    _typeobject *ob_type;
    Py_ssize_t ob_size;
    PyObject **ob_item;
    Py_ssize_t allocated;
};

struct _longobject 
{
    Py_ssize_t ob_refcnt;
    _typeobject *ob_type;
    Py_ssize_t ob_size;
    digit ob_digit[1];
};

typedef _longobject PyLongObject;

struct PySliceObject
{
    Py_ssize_t ob_refcnt;
    _typeobject *ob_type;
    PyObject *start, *stop, *step;
};

struct symtable 
{
    const char *st_filename;
};

struct _symtable_entry {
    Py_ssize_t ob_refcnt;
    _typeobject *ob_type;
    PyObject *ste_id;        
    PyObject *ste_symbols; 
    PyObject *ste_name;     
    PyObject *ste_varnames; 
    PyObject *ste_children;  
    _block_type ste_type;   
    int ste_unoptimized;   
    int ste_nested;    
    unsigned ste_free : 1;
    unsigned ste_child_free : 1;
    unsigned ste_generator : 1;
    unsigned ste_varargs : 1; 
    unsigned ste_varkeywords : 1; 
    unsigned ste_returns_value : 1;
    int ste_lineno;
    int ste_opt_lineno; 
    int ste_tmpname;
    symtable *ste_table;
};

typedef _symtable_entry PySTEntryObject;

struct symtable 
{
    const char *st_filename;
    _symtable_entry *st_cur; 
    _symtable_entry *st_top; 
    PyObject *st_symbols;   
    PyObject *st_stack;    
    PyObject *st_global;   
    int st_nblocks;        
    PyObject *st_private;       
    PyFutureFeatures *st_future;
};

struct PyUnicodeObject
{
    Py_ssize_t ob_refcnt;
    _typeobject *ob_type;
    Py_ssize_t length;
    Py_UNICODE *str;
    long hash;
    PyObject *defenc;
};

struct _PyWeakReference 
{
    Py_ssize_t ob_refcnt;
};

typedef _PyWeakReference PyWeakReference;

struct _PyWeakReference 
{
    Py_ssize_t ob_refcnt;
    _typeobject *ob_type;
    PyObject *wr_object;
    PyObject *wr_callback;
    long hash;
    PyWeakReference *wr_prev;
    PyWeakReference *wr_next;
};

typedef void (*PyOS_sighandler_t)(int);

struct WFILE
{
    FILE *fp;
    int error;
    int depth;
    PyObject *str;
    char *ptr;
    char *end;
    PyObject *strings;
    int version;
};

typedef WFILE RFILE;

struct PyFileObject
{
    Py_ssize_t ob_refcnt;
    _typeobject *ob_type;
    FILE *f_fp;
    PyObject *f_name;
    PyObject *f_mode;
    int (*f_close)(FILE *);
    int f_softspace;           
    int f_binary;             
    char* f_buf;                
    char* f_bufend;            
    char* f_bufptr;            
    char *f_setbuf;            
    int f_univ_newline;    
    int f_newlinetypes;        
    int f_skipnextlf;        
    PyObject *f_encoding;
    PyObject *f_errors;
    PyObject *weakreflist; 
    int unlocked_count;
    int readable;
    int writable;
};

struct _inittab {
    char *name;
    void (*initfunc)(void);
};

struct filedescr {
    char *suffix;
    char *mode;
    enum filetype type;
};

typedef off_t Py_off_t;
