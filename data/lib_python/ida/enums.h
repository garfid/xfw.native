enum _expr_context 
{
    Load=1, 
    Store=2, 
    Del=3, 
    AugLoad=4, 
    AugStore=5,
    Param=6 
};

enum _unaryop 
{ 
    Invert=1, 
    Not=2, 
    UAdd=3, 
    USub=4 
};

enum _operator 
{ 
    Add=1,
    Sub=2, 
    Mult=3, 
    Div=4, 
    Mod=5,
    Pow=6, 
    LShift=7,
    RShift=8, 
    BitOr=9, 
    BitXor=10,
    BitAnd=11, 
    FloorDiv=12 
};

enum _boolop 
{ 
    And=1, 
    Or=2 
};

enum _expr_kind 
{
  BoolOp_kind=1, 
  BinOp_kind=2, 
  UnaryOp_kind=3, 
  Lambda_kind=4,
  IfExp_kind=5, 
  Dict_kind=6, 
  Set_kind=7, 
  ListComp_kind=8,
  SetComp_kind=9, 
  DictComp_kind=10, 
  GeneratorExp_kind=11,
  Yield_kind=12, 
  Compare_kind=13, 
  Call_kind=14, 
  Repr_kind=15,
  Num_kind=16, 
  Str_kind=17, 
  Attribute_kind=18,
  Subscript_kind=19, 
  Name_kind=20, 
  List_kind=21, 
  Tuple_kind=22
};

enum PyGILState_STATE
{
  PyGILState_LOCKED = 0x0,
  PyGILState_UNLOCKED = 0x1,
};

enum _slice_kind 
{
    Ellipsis_kind=1,
    Slice_kind=2, 
    ExtSlice_kind=3, 
    Index_kind=4
};

enum _stmt_kind 
{
    FunctionDef_kind=1, 
    ClassDef_kind=2, 
    Return_kind=3,
    Delete_kind=4, 
    Assign_kind=5, 
    AugAssign_kind=6, 
    Print_kind=7,
    For_kind=8, 
    While_kind=9, 
    If_kind=10, 
    With_kind=11,
    Raise_kind=12, 
    TryExcept_kind=13,
    TryFinally_kind=14,
    Assert_kind=15,
    Import_kind=16, 
    ImportFrom_kind=17,
    Exec_kind=18, 
    Global_kind=19, 
    Expr_kind=20, 
    Pass_kind=21,
    Break_kind=22, 
    Continue_kind=23
};

enum _mod_kind 
{
    Module_kind=1, 
    Interactive_kind=2,
    Expression_kind=3,
    Suite_kind=4
};

enum _block_type
{
  FunctionBlock = 0x0,
  ClassBlock = 0x1,
  ModuleBlock = 0x2,
};

enum filetype {
    SEARCH_ERROR,
    PY_SOURCE,
    PY_COMPILED,
    C_EXTENSION,
    PY_RESOURCE, /* Mac only */
    PKG_DIRECTORY,
    C_BUILTIN,
    PY_FROZEN,
    PY_CODERESOURCE, /* Mac only */
    IMP_HOOK
};
