#define Py_BUILD_CORE 1
#define DISCARD_NOTFOUND 0
#define DISCARD_FOUND 1

#include "Python.h"

#undef PySet_Contains
PyAPI_FUNC(int) PySet_Contains(PyObject *anyset, PyObject *key);

extern "C" size_t dummy;

static int
set_contains_key(PySetObject *so, PyObject *key)
{
    long hash;
    setentry *entry;

    if (!PyString_CheckExact(key) ||
        (hash = ((PyStringObject *) key)->ob_shash) == -1) {
        hash = PyObject_Hash(key);
        if (hash == -1)
            return -1;
    }
    entry = (so->lookup)(so, key, hash);
    if (entry == NULL)
        return -1;
    key = entry->key;
    return key != NULL && key != *reinterpret_cast<PyObject**>(dummy);
}

int
PySet_Contains(PyObject *anyset, PyObject *key)
{
    if (!PyAnySet_Check(anyset)) {
        PyErr_BadInternalCall();
        return -1;
    }
    return set_contains_key((PySetObject *)anyset, key);
}