#define Py_BUILD_CORE 1
#include "Python.h"
#include "PythonInternal.h"

PyObject *
PyBuffer_FromReadWriteMemory(void *ptr, Py_ssize_t size)
{
    return buffer_from_memory(NULL, size, 0, ptr, 0);
}