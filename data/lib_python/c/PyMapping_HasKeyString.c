#define Py_BUILD_CORE 1
#include "Python.h"

PyAPI_FUNC(int) PyMapping_HasKeyString(PyObject *o, char *key);

int
PyMapping_HasKeyString(PyObject *o, char *key)
{
    PyObject *v;

    v = PyMapping_GetItemString(o, key);
    if (v) {
        Py_DECREF(v);
        return 1;
    }
    PyErr_Clear();
    return 0;
}