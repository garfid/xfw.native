#define Py_BUILD_CORE 1
#include "Python.h"

PyAPI_FUNC(int) PyMapping_HasKey(PyObject *o, PyObject *key);

int
PyMapping_HasKey(PyObject *o, PyObject *key)
{
    PyObject *v;

    v = PyObject_GetItem(o, key);
    if (v) {
        Py_DECREF(v);
        return 1;
    }
    PyErr_Clear();
    return 0;
}
