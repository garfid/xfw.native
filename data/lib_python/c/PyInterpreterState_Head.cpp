#define Py_BUILD_CORE 1

#include "Python.h"
#include "pythread.h"

extern "C" size_t interp_head;

PyInterpreterState *
PyInterpreterState_Head(void)
{
    return *reinterpret_cast<PyInterpreterState**>(interp_head);
}