#define Py_BUILD_CORE 1

#include "Python.h"
#include "pythread.h"

extern "C" size_t interpreter_lock;

void
PyEval_AcquireLock(void)
{
    PyThread_acquire_lock(*reinterpret_cast<PyThread_type_lock*>(interpreter_lock), 1);
}