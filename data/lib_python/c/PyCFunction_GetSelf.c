#define Py_BUILD_CORE 1
#include "Python.h"

PyAPI_FUNC(PyObject *) PyCFunction_GetSelf(PyObject *);

PyObject *
PyCFunction_GetSelf(PyObject *op)
{
    if (!PyCFunction_Check(op)) {
        PyErr_BadInternalCall();
        return NULL;
    }
    return ((PyCFunctionObject *)op) -> m_self;
}