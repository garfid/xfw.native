#define Py_BUILD_CORE 1
#include "Python.h"

#undef _PyObject_GC_UnTrack
PyAPI_FUNC(void) _PyObject_GC_UnTrack(PyObject *);

void
_PyObject_GC_UnTrack(PyObject *op)
{
    PyObject_GC_UnTrack(op);
}