#define Py_BUILD_CORE 1

#include "Python.h"
#include "PythonInternal.h"

PyObject *PyCodec_StreamWriter(const char *encoding,
                               PyObject *stream,
                               const char *errors)
{
    return codec_getstreamcodec(encoding, stream, errors, 3);
}