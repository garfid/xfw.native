#define Py_BUILD_CORE 1
#include "Python.h"

PyAPI_FUNC(const char*) PyUnicode_GetDefaultEncoding(void);

const char*
PyUnicode_GetDefaultEncoding(void)
{
	return "ascii";
}
