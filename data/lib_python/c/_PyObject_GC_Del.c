#define Py_BUILD_CORE 1
#include "Python.h"

#undef _PyObject_GC_Del
PyAPI_FUNC(void) _PyObject_GC_Del(PyObject *);


void
_PyObject_GC_Del(PyObject *op)
{
    PyObject_GC_Del(op);
}
