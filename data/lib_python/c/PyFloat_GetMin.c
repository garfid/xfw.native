#define Py_BUILD_CORE 1
#include "Python.h"

PyAPI_FUNC(double) PyFloat_GetMin(void);

double
PyFloat_GetMin(void)
{
    return DBL_MIN;
}