#define Py_BUILD_CORE 1

#include "Python.h"
#include "PythonInternal.h"

PyAPI_FUNC(void) PySys_WriteStdout(const char *format, ...);

void
PySys_WriteStdout(const char *format, ...)
{
    va_list va;

    va_start(va, format);
    mywrite("stdout", stdout, format, va);
    va_end(va);
}
