#define Py_BUILD_CORE 1
#include "Python.h"

#undef _PyObject_GC_Track
PyAPI_FUNC(void) _PyObject_GC_Track(PyObject *);


void
_PyObject_GC_Track(PyObject *op)
{
    PyObject_GC_Track(op);
}