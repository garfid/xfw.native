#define Py_BUILD_CORE 1
#include "Python.h"

PyAPI_FUNC(void) PyThread_ReInitTLS(void);

/* reinitialization of TLS is not necessary after fork when using
 * the native TLS functions.  And forking isn't supported on Windows either.
 */
void
PyThread_ReInitTLS(void)
{}
