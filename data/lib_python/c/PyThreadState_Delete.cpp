#define Py_BUILD_CORE 1

#include "Python.h"
#include "PythonInternal.h"
#include <pythread.h>

extern "C" size_t autoInterpreterState;
extern "C" size_t autoTLSkey;

void
PyThreadState_Delete(PyThreadState *tstate)
{
    if (tstate == _PyThreadState_Current)
        Py_FatalError("PyThreadState_Delete: tstate is still current");
 
    tstate_delete_common(tstate);

    if (*reinterpret_cast<PyInterpreterState**>(autoInterpreterState) && PyThread_get_key_value(*reinterpret_cast<int*>(autoTLSkey)) == tstate)
        PyThread_delete_key_value(*reinterpret_cast<int*>(autoTLSkey));
}