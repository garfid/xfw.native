#define Py_BUILD_CORE 1
#include "Python.h"

typedef struct {
    PyObject_HEAD
    PyObject *b_base;
    void *b_ptr;
    Py_ssize_t b_size;
    Py_ssize_t b_offset;
    int b_readonly;
    long b_hash;
} PyBufferObject;

PyObject *
PyBuffer_New(Py_ssize_t size)
{
    PyObject *o;
    PyBufferObject * b;

    if (size < 0) {
        PyErr_SetString(PyExc_ValueError,
                        "size must be zero or positive");
        return NULL;
    }
    if (sizeof(*b) > PY_SSIZE_T_MAX - size) {
        /* unlikely */
        return PyErr_NoMemory();
    }
    /* Inline PyObject_New */
    o = (PyObject *)PyObject_MALLOC(sizeof(*b) + size);
    if ( o == NULL )
        return PyErr_NoMemory();
    b = (PyBufferObject *) PyObject_INIT(o, &PyBuffer_Type);

    b->b_base = NULL;
    b->b_ptr = (void *)(b + 1);
    b->b_size = size;
    b->b_offset = 0;
    b->b_readonly = 0;
    b->b_hash = -1;

    return o;
}