#define Py_BUILD_CORE 1
#include "Python.h"
#include "PythonInternal.h"

PyAPI_FUNC(PyObject *) PyMarshal_ReadObjectFromFile(FILE *);

PyObject *
PyMarshal_ReadObjectFromFile(FILE *fp)
{
    RFILE rf;
    PyObject *result;
    rf.fp = fp;
    rf.strings = PyList_New(0);
    rf.depth = 0;
    rf.ptr = rf.end = NULL;
    result = r_object(&rf);
    Py_DECREF(rf.strings);
    return result;
};