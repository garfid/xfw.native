#define Py_BUILD_CORE 1
#include "Python.h"
#include <limits.h>

PyAPI_FUNC(long) PyInt_GetMax(void);

long
PyInt_GetMax(void)
{
    return LONG_MAX;            /* To initialize sys.maxint */
}