#define Py_BUILD_CORE 1
#include "Python.h"

PyAPI_FUNC(PyObject*) PyUnicode_AsASCIIString(
    PyObject *unicode           /* Unicode object */
    );

PyObject *PyUnicode_AsASCIIString(PyObject *unicode)
{
    if (!PyUnicode_Check(unicode)) {
        PyErr_BadArgument();
        return NULL;
    }
    return PyUnicode_EncodeASCII(PyUnicode_AS_UNICODE(unicode),
                                 PyUnicode_GET_SIZE(unicode),
                                 NULL);
}
