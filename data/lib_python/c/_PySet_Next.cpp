#define Py_BUILD_CORE 1
#define DISCARD_NOTFOUND 0
#define DISCARD_FOUND 1

#include "Python.h"

#undef _PySet_Next
PyAPI_FUNC(int) _PySet_Next(PyObject *set, Py_ssize_t *pos, PyObject **key);

extern "C"  size_t dummy;

static int
set_next(PySetObject *so, Py_ssize_t *pos_ptr, setentry **entry_ptr)
{
    Py_ssize_t i;
    Py_ssize_t mask;
    register setentry *table;

    assert (PyAnySet_Check(so));
    i = *pos_ptr;
    assert(i >= 0);
    table = so->table;
    mask = so->mask;
    while (i <= mask && (table[i].key == NULL || table[i].key == *reinterpret_cast<PyObject**>(dummy)))
        i++;
    *pos_ptr = i+1;
    if (i > mask)
        return 0;
    assert(table[i].key != NULL);
    *entry_ptr = &table[i];
    return 1;
}


int
_PySet_Next(PyObject *set, Py_ssize_t *pos, PyObject **key)
{
    setentry *entry_ptr;

    if (!PyAnySet_Check(set)) {
        PyErr_BadInternalCall();
        return -1;
    }
    if (set_next((PySetObject *)set, pos, &entry_ptr) == 0)
        return 0;
    *key = entry_ptr->key;
    return 1;
}