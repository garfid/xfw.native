#define Py_BUILD_CORE 1
#include "Python.h"

#include "pythread.h"

PyInterpreterState *
PyInterpreterState_Next(PyInterpreterState *interp) {
    return interp->next;
}