#define Py_BUILD_CORE 1
#include "Python.h"
#include "structmember.h"

static PyObject *
listmembers(struct memberlist *mlist)
{
    int i, n;
    PyObject *v;
    for (n = 0; mlist[n].name != NULL; n++)
        ;
    v = PyList_New(n);
    if (v != NULL) {
        for (i = 0; i < n; i++)
            PyList_SetItem(v, i,
                           PyString_FromString(mlist[i].name));
        if (PyErr_Occurred()) {
            Py_DECREF(v);
            v = NULL;
        }
        else {
            PyList_Sort(v);
        }
    }
    return v;
}


PyObject *
PyMember_Get(const char *addr, struct memberlist *mlist, const char *name)
{
    struct memberlist *l;

    if (strcmp(name, "__members__") == 0)
        return listmembers(mlist);
    for (l = mlist; l->name != NULL; l++) {
        if (strcmp(l->name, name) == 0) {
            PyMemberDef copy;
            copy.name = l->name;
            copy.type = l->type;
            copy.offset = l->offset;
            copy.flags = l->flags;
            copy.doc = NULL;
            return PyMember_GetOne(addr, &copy);
        }
    }
    PyErr_SetString(PyExc_AttributeError, name);
    return NULL;
}