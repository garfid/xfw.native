#define Py_BUILD_CORE 1

#include "Python.h"
#include "pythread.h"

extern "C" size_t interpreter_lock;

int
PyEval_ThreadsInitialized(void)
{
    return (*reinterpret_cast<PyThread_type_lock*>(interpreter_lock)) != 0;
}