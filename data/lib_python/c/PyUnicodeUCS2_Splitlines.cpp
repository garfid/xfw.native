#define Py_BUILD_CORE 1

#include "Python.h"
#include "Initializer.h"
#include "unicodeobject.h"

static PyUnicodeObject *unicode_empty = NULL;

static unsigned char ascii_linebreak[] = {
    0, 0, 0, 0, 0, 0, 0, 0,
/*         0x000A, * LINE FEED */
/*         0x000B, * LINE TABULATION */
/*         0x000C, * FORM FEED */
/*         0x000D, * CARRIAGE RETURN */
    0, 0, 1, 1, 1, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
/*         0x001C, * FILE SEPARATOR */
/*         0x001D, * GROUP SEPARATOR */
/*         0x001E, * RECORD SEPARATOR */
    0, 0, 0, 0, 1, 1, 1, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,

    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0
};

#if LONG_BIT >= 128
    #define BLOOM_WIDTH 128
#elif LONG_BIT >= 64
    #define BLOOM_WIDTH 64
#elif LONG_BIT >= 32
    #define BLOOM_WIDTH 32
#else
    #error "LONG_BIT is smaller than 32"
#endif

#define BLOOM_MASK unsigned long

static BLOOM_MASK bloom_linebreak = ~(BLOOM_MASK)0;

#define BLOOM(mask, ch)     ((mask &  (1UL << ((ch) & (BLOOM_WIDTH - 1)))))

#define BLOOM_LINEBREAK(ch)                                             \
    ((ch) < 128U ? ascii_linebreak[(ch)] :                              \
     (BLOOM(bloom_linebreak, (ch)) && Py_UNICODE_ISLINEBREAK(ch)))

#include "stringlib/unicodedefs.h"
#include "stringlib/fastsearch.h"

#include "stringlib/count.h"
#include "stringlib/find.h"
#include "stringlib/partition.h"
#include "stringlib/split.h"

PyObject *PyUnicodeUCS2_Splitlines(PyObject *string, int keepends)
{
    if(bloom_linebreak == ~(BLOOM_MASK)0)
        bloom_linebreak = *reinterpret_cast<unsigned long*>(Initializer::GetStructureRealAddress("bloom_linebreak"));

   	if (unicode_empty == NULL)
		unicode_empty = *reinterpret_cast<PyUnicodeObject**>(Initializer::GetStructureRealAddress("unicode_empty"));

    PyObject *list;

    string = PyUnicode_FromObject(string);
    if (string == NULL)
        return NULL;

    list = stringlib_splitlines(
        (PyObject*) string, PyUnicode_AS_UNICODE(string),
        PyUnicode_GET_SIZE(string), keepends);

    Py_DECREF(string);
    return list;
}