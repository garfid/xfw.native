# IDA utilities
# @author ShadowHunterRus
# @author Mikhail Paulyshka
# @license GPLv3

from idaapi import *
from idautils import *
from idc import *

from win32api import GetFileVersionInfo, LOWORD, HIWORD

def IsValidEA(x):
    return x != 0 and x != BADADDR


def IsGoodSig(sig):
    addr, count = 0, 0
    addr = FindBinary(addr, SEARCH_DOWN | SEARCH_NEXT, sig)
    while (count <= 2) and (addr != BADADDR):
        count += 1
        addr = FindBinary(addr, SEARCH_DOWN | SEARCH_NEXT, sig)
    return (count == 1)


def AddBytesToSig(dwAddress, dwSize):
    strSig = ''
    for i in xrange(dwSize):
        strSig += '%02X ' % get_byte(dwAddress + i)
    return strSig


def AddWhiteSpacesToSig(dwSize):
    return '? ' * dwSize


def getCurrentOpcodeSize(uiCount, pInfo):
    for i in xrange(UA_MAXOP):
        uiCount = i
        if pInfo.Operands[i].type == 14:
            return 0
        if pInfo.Operands[i].offb != 0:
            return pInfo.Operands[i].offb
    return 0


def MatchOperands(uiOperand, uiSize, pInfo):
    if get_first_dref_from(pInfo.ea) != BADADDR:
        return False
    if get_first_cref_from(pInfo.ea) != BADADDR:
        return False
    return True


def AddInsToSig(pInfo):
    uiCount = 0
    uiSize = getCurrentOpcodeSize(uiCount, pInfo)
    strSig = ''
    if uiSize == 0:
        return AddBytesToSig(pInfo.ea, pInfo.size)
    else:
        strSig += AddBytesToSig(pInfo.ea, uiSize)
    if MatchOperands(0, uiSize, pInfo):
        strSig += AddBytesToSig(pInfo.ea + uiSize, pInfo.size - uiSize)
    else:
        strSig += AddWhiteSpacesToSig(pInfo.size - uiSize)
    return strSig


def getSig(addr):
    strSig = ''
    found = False
    pAddress = GetFunctionAttr(addr, FUNCATTR_START)
    assert pAddress != BADADDR, 'Make sure you are in a function!'
    pFunctionEnd = GetFunctionAttr(pAddress, FUNCATTR_END)
    while pAddress != BADADDR:
        pInfo = DecodeInstruction(pAddress)
        if pInfo is None:
            return
        if pInfo.size < 5:
            strSig += AddBytesToSig(pInfo.ea, pInfo.size)
        else:
            strSig += AddInsToSig(pInfo)
        if IsGoodSig(strSig):
            found = True
            break
        pAddress = NextHead(pAddress, pFunctionEnd)
    return strSig.strip() if found else None

def getFileVersion():
    info = GetFileVersionInfo(GetInputFilePath(),"\\")
    ms = info['FileVersionMS']
    ls = info['FileVersionLS']
    return str(HIWORD(ms))+'.'+str(LOWORD(ms))+'.'+str(HIWORD(ls))+'.'+str(LOWORD(ls))
