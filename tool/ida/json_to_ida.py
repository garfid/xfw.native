# JSON to IDA script
# @author ShadowHunterRus
# @author Mikhail Paulyshka
# @license GPLv3

import json
import os

import idc
import ida_typeinf

import sigutils
reload(sigutils)

jsonfolder = os.path.dirname(os.path.dirname(__file__))+"../../data/lib_python/json/"
ida_types_file = os.path.dirname(os.path.dirname(__file__))+"../../data/lib_python/ida/types.h"
ida_enums_file = os.path.dirname(os.path.dirname(__file__))+"../../data/lib_python/ida/enums.h"
ida_offsets_file = os.path.dirname(os.path.dirname(__file__))+"../../data/lib_python/json/_offsets.json"

def import_typedef(str):
    tif = ida_typeinf.tinfo_t()
    til = ida_typeinf.get_idati()

    name = ida_typeinf.parse_decl(tif, til, str, 0)
    if not name:
        print(str)
        print('  error when parse decl')
        return

    ordinal = ida_typeinf.get_type_ordinal(til, name)
    if ordinal < 1:
        ordinal = ida_typeinf.alloc_type_ordinal(til)

    res = tif.set_numbered_type(til, ordinal, ida_typeinf.NTF_REPLACE, name)
    if res == -1:
        print('  error when setting numbered type')
        return

    ida_typeinf.import_type(til, -1, name, ida_typeinf.IMPTYPE_OVERRIDE)


def set_function_typedef(offset, wot_function):
    if len(wot_function['ReturnType']) == 0:
        print('ERROR ret: %s' % wot_function['Name'])
        return
    if len(wot_function['Arguments']) == 0:
        print('ERROR arg: %s' % wot_function['Name'])
        return

    function_prototype = "%s %s(%s);" % (wot_function['ReturnType'], wot_function['Name'], wot_function['Arguments'])
   
    if not idc.SetType(offset, str(function_prototype)):
        print('typedef error with function: %s' % function_prototype)


def set_structure_typedef(offset, wot_structure):
    if len(wot_structure['Type']) == 0:
        print('ERROR ret: %s' % wot_structure['Name'])
        return

    struct_type = wot_structure['Type']
    if struct_type.endswith('[]'):
        structure_prototype = "%s %s[];" % (struct_type[:-2],  wot_structure['Name'])
    else:
        structure_prototype = "%s %s;" % (struct_type,  wot_structure['Name'])
   
    if not idc.SetType(offset, str(structure_prototype)):
        print('typedef error with structure: %s' % structure_prototype)


def import_typedef_file(filename):
   with open(filename) as f:
    for definition in f.read().replace("\r\n", "\n").split("\n\n"):
       import_typedef(definition)


wot_found = False
try:
    wotversion = sigutils.getFileVersion()

    print('')
    print('WoT Version:'+wotversion)

    wot_found = True
except:
    print('WoT exe not found')
    pass

#import local types
import_typedef_file(ida_enums_file)
import_typedef_file(ida_types_file)

if wot_found:
    #mark names
    offsets = None
    with open(ida_offsets_file, 'r') as f:
        json_file = json.load(f)
        offsets = json_file['Offsets'][wotversion]

        for key, value in offsets.iteritems():
            idc.MakeName(int(value, 16), str(key))

    #mark types
    for filename in os.listdir(jsonfolder):
        if filename.endswith("_offsets.json"):
            continue

        if not filename.endswith(".json"):
            continue
        
        json_file = None
        with open(jsonfolder+filename, 'r') as file:
            json_file = json.load(file)

        for wot_function in json_file['Functions']:
            if wot_function['Name'] in offsets:
                set_function_typedef(int(offsets[wot_function['Name']],16), wot_function)

        for wot_structure in json_file['Structures']:
            if wot_structure['Name'] in offsets:
                set_structure_typedef(int(offsets[wot_structure['Name']],16), wot_structure)

    print('Finished importing WoT Python types')

print('==============')
