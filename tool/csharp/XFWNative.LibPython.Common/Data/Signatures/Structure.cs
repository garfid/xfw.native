﻿using System.Collections.Generic;

namespace XFWNative.Libpython.Common.Data.Signatures
{
    public class Structure
    {
        public string Name { get; set; }

        public string Type { get; set; }

        public List<StructureSignature> Signatures_32bit { get; set; } = new List<StructureSignature>();

        public List<StructureSignature> Signatures_64bit { get; set; } = new List<StructureSignature>();

        public List<StructureSignature> GetSignatures(int bitness)
        {
            return bitness == 64 ? Signatures_64bit : Signatures_32bit;
        }

        public Structure()
        {
        }

    }
}
