﻿using System;
using System.Collections.Generic;

namespace XFWNative.Libpython.Common.Data.Signatures
{
    public class Function
    {
        public string Name;
        public string Arguments;
        public string ReturnType;

        public bool IgnoreFromHeader { get; set; } = false;

        [NonSerialized]
        public bool IsInternal = false;

        [NonSerialized]
        public bool Inlined_32bit = false;

        [NonSerialized]
        public bool Inlined_64bit = false;


        public List<FunctionSignature> Signatures_32bit = new List<FunctionSignature>();

        public List<FunctionSignature> Signatures_64bit = new List<FunctionSignature>();

        public bool GetInlined(int bitness)
        {
            return bitness == 64 ? Inlined_64bit : Inlined_32bit;
        }

        public void SetInlined(int bitness, bool state)
        {
            if(bitness == 32 )
            {
                Inlined_32bit = state;
            }

            if(bitness == 64)
            {
                Inlined_64bit = state;
            }
        }

        public List<FunctionSignature> GetSignatures(int bitness)
        {
            return bitness == 64 ? Signatures_64bit : Signatures_32bit;
        }
    }
}
