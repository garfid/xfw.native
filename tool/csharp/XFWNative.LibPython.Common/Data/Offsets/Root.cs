﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace XFWNative.Libpython.Common.Data.Offsets
{
    public class Root
    {
        public Dictionary<string, ConcurrentDictionary<string,string>> Offsets { get; } = new Dictionary<string, ConcurrentDictionary<string, string>>();

        public Root() { }
    }
}
