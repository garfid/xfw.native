﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Newtonsoft.Json;

namespace XFWNative.Libpython.Common
{
    public class DB
    {
        #region Properties

        private Dictionary<string, Data.Signatures.Root> DataSignatures { get; } = new Dictionary<string, Data.Signatures.Root>();
        public KeyValuePair<string, Data.Offsets.Root> DataOffsets { get; private set; }
        
        public List<string> InlinedFunctions { get;  } = new List<string>();

        private string DataFolder { get; }
        private string JsonFolder { get; }
        private string FunctionsFolder { get; }

        #endregion

        #region Initialization

        public DB(string folder)
        {
            DataFolder = folder;
            JsonFolder = Path.Combine(DataFolder, "json\\");
            FunctionsFolder = Path.Combine(DataFolder, "c\\");

            LoadInlineNames();
            LoadJsonData();
        }

        private void LoadInlineNames()
        {
            foreach (var file in Directory.GetFiles(FunctionsFolder, "*.*"))
            {
                InlinedFunctions.Add(Path.GetFileNameWithoutExtension(file));
            }
        }

        #endregion

        #region Load/Save

        private void LoadJsonData()
        {
            if (!Directory.Exists(JsonFolder))
            {
                Console.WriteLine($"ERROR: {JsonFolder} does not exists");
                return;
            }

            foreach (string file in Directory.GetFiles(JsonFolder))
            {
                if (Path.GetExtension(file).ToLower() != ".json")
                    continue;

                if (Path.GetFileName(file).ToLower() == "_offsets.json")
                {
                    DataOffsets = new KeyValuePair<string, Data.Offsets.Root>(file, JsonConvert.DeserializeObject<Common.Data.Offsets.Root>(File.ReadAllText(file)));
                }
                else
                {
                    var json = JsonConvert.DeserializeObject<Common.Data.Signatures.Root>(File.ReadAllText(file));

                    foreach (var function in json.Functions)
                    {
                        if (file.ToLower().Contains("internal"))
                            function.IsInternal = true;

                        if (InlinedFunctions.Contains(function.Name))
                        {
                            function.SetInlined(32, true);
                            function.SetInlined(64, true);
                        }
                    }

                    DataSignatures[file] = json;
                }
            }
        }

        public void SaveJsonData()
        {
            foreach (var kvp in DataSignatures)
            {
                File.WriteAllText(kvp.Key,JsonConvert.SerializeObject(kvp.Value, Formatting.Indented));
            }

            File.WriteAllText(DataOffsets.Key, JsonConvert.SerializeObject(DataOffsets.Value, Formatting.Indented));
 
        }


        #endregion

        #region Data

        public IEnumerable<Data.Signatures.Function> GetFunctions()
        {
            return DataSignatures.SelectMany(x => x.Value.Functions);
        }
        
        public IEnumerable<Data.Signatures.Structure> GetStructures()
        {
            return DataSignatures.SelectMany(x => x.Value.Structures);
        }

        public ConcurrentDictionary<string,string> GetOffsetsForVersion(string version)
        {
            if(!DataOffsets.Value.Offsets.ContainsKey(version))
                DataOffsets.Value.Offsets[version] = new ConcurrentDictionary<string, string>();

            return DataOffsets.Value.Offsets[version];
        }

        #endregion
    }
}
