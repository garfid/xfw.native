﻿using System;

namespace XFWNative.Libpython.Common
{
    public static class Helper
    {
        public static (string pattern, string mask) ProcessSignature(string inputString)
        {
            var tokens = inputString.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            
            string pattern = "";
            string mask = "";

            foreach(var token in tokens)
            {
                if ((token == "?") || (token == "XX") || (token == "??"))
                {
                    pattern += @"\x00";
                    mask += "?";
                }
                else
                {
                    pattern += @"\x" + token;
                    mask += "x";
                }
            }
            return (pattern, mask);

        }
    }
}
