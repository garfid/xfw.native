﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace XFWNative.Common.Extensions
{
    public static class ByteArrayExtensions
    {
        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        public static int IndexOf(this byte[] arrayToSearchThrough, byte[] patternToFind)
        {
            if (patternToFind.Length > arrayToSearchThrough.Length)
                return -1;

            for (var i = 0; i < arrayToSearchThrough.Length - patternToFind.Length; i++)
            {
                var found = true;

                for (var j = 0; j < patternToFind.Length; j++)
                {
                    if (arrayToSearchThrough[i + j] != patternToFind[j])
                    {
                        found = false;
                        break;
                    }
                }

                if (found)
                {
                    return i;
                }
            }
            return -1;
        }

        public static int IndexOf(this byte[] arrayToSearchThrough, byte[] patternToFind, byte[] mask)
        {
            if (patternToFind.Length > arrayToSearchThrough.Length)
                return -1;

            for (var i = 0; i < arrayToSearchThrough.Length - patternToFind.Length; i++)
            {
                var found = true;

                for (var j = 0; j < patternToFind.Length; j++)
                {
                    if (mask[j] == 1)
                    {
                        continue;
                    }

                    if (arrayToSearchThrough[i + j] != patternToFind[j])
                    {
                        found = false;
                        break;
                    }
                }

                if (found)
                {
                    return i;
                }
            }
            return -1;
        }

        public static int[] AllIndexesOf(this byte[] array, byte[] pattern)
        {
            var indexes = new List<int>();

            while (true)
            {
                var ind = array.IndexOf(pattern);
                if (ind == -1)
                {
                    return indexes.ToArray();
                }
                else
                {
                    indexes.Add(ind);
                    array = array.SubArray(ind + pattern.Length, array.Length - (ind + pattern.Length));
                }
            }
        }

        public static int[] AllIndexesOf(this byte[] array, byte[] pattern, byte[] mask)
        {
            var indexes = new List<int>();

            while (true)
            {
                var ind = array.IndexOf(pattern, mask);
                if (ind == -1)
                {
                    return indexes.ToArray();
                }
                else
                {
                    indexes.Add(ind);
                    array = array.SubArray(ind + pattern.Length, array.Length - (ind + pattern.Length));
                }
            }
        }

        public static int[] AllIndexesOf(this byte[] array, string pattern)
        {
            string[] s_spi = pattern.Split(' ');
            var b_pat = new byte[s_spi.Length];
            var b_mask = new byte[s_spi.Length];
            for (int i = 0; i < s_spi.Length; i++)
            {
                if ((s_spi[i] == "?") || (s_spi[i] == "XX") || (s_spi[i] == "??"))
                {
                    b_pat[i] = 0x00;
                    b_mask[i] = 0x01;
                }
                else
                {
                    b_pat[i] = byte.Parse(s_spi[i], NumberStyles.HexNumber, CultureInfo.InvariantCulture);
                    b_mask[i] = 0x00;
                }
            }
            return AllIndexesOf(array, b_pat, b_mask);
        }


        public static bool CheckPrefix(this byte[] array, int index, string prefix)
        {
            if (string.IsNullOrEmpty(prefix))
                return false;

            string[] s_spi = prefix.Split(' ');
            index -= s_spi.Length;

            foreach (var symbol in s_spi)
            {
                if (byte.Parse(symbol, NumberStyles.HexNumber, CultureInfo.InvariantCulture) != array[index])
                    return false;
                index++;
            }

            return true;
        }

        public static byte[] ReplaceBytes(this byte[] src, byte[] search, byte[] repl)
        {
            int index = -1;
            var dst = src;

            while((index = dst.IndexOf(search)) >= 0)
            {
                var temp = new byte[dst.Length - search.Length + repl.Length];

                Buffer.BlockCopy(dst, 0, temp, 0, index);
                Buffer.BlockCopy(repl, 0, temp, index, repl.Length);
                Buffer.BlockCopy(dst, index + search.Length, temp, index + repl.Length, temp.Length - (index + search.Length));

                dst = temp;
            }

            return dst;
        }

        public static byte[] ReplaceBytes(this byte[] src, byte[] search, byte[] repl, bool preserve_size)
        {
            if (!preserve_size)
            {
                return src.ReplaceBytes(search, repl);
            }
            
            byte[] repl_new = null;
            if (search.Length == repl.Length)
            {
                repl_new = repl;
            }
            else if(search.Length > repl.Length)
            {
                repl_new = new byte[search.Length];
                Array.Copy(repl, 0, repl_new, 0, repl.Length);
            }
            else
            {
                throw new ArgumentException();
            }

            return src.ReplaceBytes(search, repl_new);
        }
    }
}
