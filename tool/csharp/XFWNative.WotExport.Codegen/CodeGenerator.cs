﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using XFWNative.WotExport.Common;

namespace XFWNative.WotExport.Codegen
{
    internal class CodeGenerator
    {
        private string dataDirectory { get; set; }
        private string outputDirectory { get; set; }
        private int bitness { get; set; }

        public List<SymbolRecord> Symbols = new List<SymbolRecord>();

        public CodeGenerator(string dataDirectory, string outputDirectory, int bitness)
        {
            this.dataDirectory = dataDirectory;
            this.outputDirectory = outputDirectory;
            this.bitness = bitness;

            loadSymbols();
        }

        private void loadSymbols()
        {
            var filepath = Path.Combine(dataDirectory, $"{bitness}bit.dat");
            
            var filelines = File.ReadAllLines(filepath);
            foreach(var line in filelines)
            {
                Symbols.Add(new SymbolRecord(line));
            }
        }

        public void Generate()
        {
            if (bitness == 64)
            {
                
                generateAsm();
            }

            generateDef();
            generateC();
            generateH();
            
            if(bitness == 32)
            {
                generateCpp();
            }
        }

        private void generateCpp()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"#include <Windows.h>");

            foreach (var symbol in Symbols)
            {
                sb.AppendLine($"extern \"C\" size_t {symbol.FunctionSafe}_addr = 0;");
                
            }
            sb.AppendLine("");

            foreach (var symbol in Symbols)
            {
                var calling = "";
                if (symbol.Function.StartsWith('?'))
                {
                    calling = "__vectorcall";
                }

                sb.AppendLine($"__declspec(naked) void {calling} {symbol.FunctionSafe}(){{");
                sb.AppendLine("    __asm {");
                sb.AppendLine($"         jmp {symbol.FunctionSafe}_addr");
                sb.AppendLine("    }");
                sb.AppendLine("}");
                if (symbol.Function != symbol.FunctionSafe)
                {
                //    sb.AppendLine($"#pragma comment(linker, \"/EXPORT:{symbol.Function}={symbol.FunctionSafe}\")");
                }
                sb.AppendLine("");
            }
      
            File.WriteAllText(Path.Combine(outputDirectory, $"wotexport_{bitness}bit.cpp"), sb.ToString());
        }

        private void generateH()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"#pragma once");
            sb.AppendLine("void WotExport_FillAddr();");

            File.WriteAllText(Path.Combine(outputDirectory, $"wotexport_{bitness}bit.h"), sb.ToString());
        }

        private void generateC()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"#include <Windows.h>");
            sb.AppendLine($"#include \"wotexport_{bitness}bit.h\"");
            sb.AppendLine("");


            foreach (var symbol in Symbols)
            {
                    sb.AppendLine($"extern size_t {symbol.FunctionSafe}_addr;");
            }
            sb.AppendLine("");

            sb.AppendLine("void WotExport_FillAddr(){");
            
            sb.AppendLine("    const HMODULE handle = GetModuleHandleA(NULL);");
            sb.AppendLine("");

            foreach (var symbol in Symbols)
            {
                sb.AppendLine($"    {symbol.FunctionSafe}_addr=(size_t)GetProcAddress(handle,\"{symbol.Function}\");");
            }
            sb.AppendLine("}");

            File.WriteAllText(Path.Combine(outputDirectory, $"wotexport_{bitness}bit.c"), sb.ToString());
        }

        private void generateDef()
        {
            var sb = new StringBuilder();
            sb.AppendLine("EXPORTS");
    
            foreach (var symbol in Symbols) {
                if (bitness == 32 && symbol.Function.StartsWith('?'))
                {
                    sb.AppendLine($"{symbol.Function.Substring(0).Replace('@', '^')}={symbol.FunctionSafe}");
                }
                else
                {
                    sb.AppendLine($"{symbol.Function.Replace('@', '^')}={symbol.FunctionSafe}");
                }
            }

            sb.AppendLine("initxfw_wotexport");

            File.WriteAllText(Path.Combine(outputDirectory, $"wotexport_{bitness}bit.def"), sb.ToString());
        }

        private void generateAsm()
        {
            var sb = new StringBuilder();
            
            //model
            if(bitness == 32)
            {
                sb.AppendLine(".model flat, C");
            }
            sb.AppendLine("");

            //public
            foreach (var symbol in Symbols)
            {
                    sb.AppendLine($"PUBLIC {symbol.FunctionSafe}_addr");
            }
            sb.AppendLine("");

            //data
            sb.AppendLine(".data");
            foreach (var symbol in Symbols)
            {
                if (bitness == 64)
                {

                    sb.AppendLine($"{symbol.FunctionSafe}_addr QWORD 0");
                }
                else
                {
                    sb.AppendLine($"{symbol.FunctionSafe}_addr DWORD 0");
                }
            }
            sb.AppendLine("");
            
            //code
            sb.AppendLine(".code");
            sb.AppendLine("");
            foreach(var symbol in Symbols)
            {
                sb.AppendLine($"{symbol.FunctionSafe} PROC");
                sb.AppendLine($"    jmp {symbol.FunctionSafe}_addr");
                sb.AppendLine($"{symbol.FunctionSafe} ENDP");
                sb.AppendLine("");
            }
            sb.AppendLine("");

            sb.AppendLine("END");


            File.WriteAllText(Path.Combine(outputDirectory, $"wotexport_{bitness}bit.asm"), sb.ToString());
        }
    }
}