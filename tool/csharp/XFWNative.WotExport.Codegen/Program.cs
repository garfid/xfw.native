﻿using CommandLine;

namespace XFWNative.WotExport.Codegen
{
    class Program
    {
        static void Main(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments<Options>(args)
              .WithParsed<Options>(opts => RunOptionsAndReturnExitCode(opts));
        }

        private static void RunOptionsAndReturnExitCode(Options opts)
        {
            new CodeGenerator(opts.DataDirectory, opts.OutputDirectory, 32).Generate();
            new CodeGenerator(opts.DataDirectory, opts.OutputDirectory, 64).Generate();
        }
    }
}
