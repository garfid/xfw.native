﻿using CommandLine;

namespace XFWNative.WotExport.Patcher
{
    class Program
    {
        static void Main(string[] args)
        {
            CommandLine.Parser.Default.ParseArguments<Options>(args)
              .WithParsed<Options>(opts => RunOptionsAndReturnExitCode(opts));
        }

        private static void RunOptionsAndReturnExitCode(Options opts)
        {
            new Patcher(opts.SymbolFile, opts.BinaryFile).Patch();
        }
    }
}