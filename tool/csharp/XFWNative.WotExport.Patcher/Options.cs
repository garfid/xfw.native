﻿using CommandLine;

namespace XFWNative.WotExport.Patcher
{
    class Options
    {
        [Option('s', "symbol_file", Required = true, HelpText = "Symbol file.")]
        public string SymbolFile { get; set; }

        [Option('b', "binary_file", Required = true, HelpText = "Binary file.")]
        public string BinaryFile { get; set; }
    }
}
