﻿namespace XFWNative.WotExport.Common
{
    public class SymbolRecord
    {
        public string Ordinal { get; } = null;
        public string Hint { get; } = null;
        public string Function { get; } = null;
        public string VirtualAddress { get; } = null;
        public string Demangler { get; } = null;
        public string FunctionSafe { get
            {
                return Function
                    .Replace("@", "_")
                    .Replace("?", "_")
                    .Replace("$", "_")
                    .Replace(":", "_");
            }
        }

        public SymbolRecord(string str)
        {
            var tokens = str.Split(',');
            Ordinal = tokens[0].Trim();
            Hint = tokens[1].Trim();
            Function = tokens[2].Trim();
            VirtualAddress = tokens[3].Trim();
            Demangler = tokens[4].Trim();
        }
    }
}
