﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Text;

namespace XFWNative.Libpython.Codegen
{
    class Options
    {

        [Option('d', "data_dir", Required = true, HelpText = "Data Directory.")]
        public string DataDirectory { get; set; }

        [Option('o', "output_dir", Required = true, HelpText = "Output Directory.")]
        public string OutputDirectory { get; set; }

        [Option('t', "template_dir", Required = true, HelpText = "Template Directory.")]
        public string TemplateDirectory { get; set; }

    }
}
