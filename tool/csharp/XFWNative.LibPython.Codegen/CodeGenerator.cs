﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

using XFWNative.Libpython.Common;

namespace XFWNative.Libpython.Codegen
{
    public class CodeGenerator
    {
        private int bitness { get; set; } = 0;

        private DB db { get; set; } = null;

        private string templateDir { get; set; } = null;

        private string outputDir { get; set; } = null;

        private Dictionary<string,StringBuilder> macroses { get;} = new Dictionary<string, StringBuilder>();

        public CodeGenerator(string dataDir, string templateDir, string outputDir, int bitness)
        {
            this.bitness = bitness;
            db = new DB(dataDir); 
            
            this.templateDir = templateDir;
            this.outputDir = outputDir;


            processAsm();
            ProcessFunctions();
            ProcessStructures();
        }


        void appendMacroLine(string macro, string text)
        {
            if (!macroses.ContainsKey(macro))
                macroses[macro] = new StringBuilder();

            macroses[macro].AppendLine(text);
        }


        void appendMacro(string macro, string text)
        {
            if (!macroses.ContainsKey(macro))
                macroses[macro] = new StringBuilder();

            macroses[macro].Append(text);
        }


        void processAsm()
        {
            if (bitness == 64)
            {
                appendMacroLine("MASM_MODEL", "");
            }
            else
            {
                appendMacroLine("MASM_MODEL", ".model flat, C");
            }
        }

        void ProcessFunctions()
        {
            foreach (var rec in db.GetFunctions().Where(x => x.IsInternal && !x.IgnoreFromHeader))
            {
                appendMacroLine("FUNCTIONS_INTERNAL_DECLARATION", $"{rec.ReturnType} {rec.Name}({rec.Arguments});");
            }

            int functionIndex = -1;
            int maxSignaturesCount = 0;

            foreach (var func in db.GetFunctions())
            {
                //get signature count
                var signatures = func.GetSignatures(bitness);
                var signature_count = signatures.Count;

                //update max signature counter
                if (maxSignaturesCount < signature_count)
                {
                    maxSignaturesCount = signature_count;
                }

                var is_inlined = func.GetInlined(bitness);

                if (signature_count == 0)
                    continue;

                if (is_inlined == true)
                    continue;

                functionIndex++;

                appendMacroLine("FUNCTIONS_NAMES", $"    funcenum_{func.Name}={functionIndex},");

                appendMacroLine("FUNCTIONS_INITIALIZE_NAMES", $"    \"{func.Name}\",");
                appendMacroLine("FUNCTIONS_INITIALIZE_SIGNATURE_COUNT", $"    {signature_count},");

                //fill signature array
                appendMacroLine("FUNCTIONS_INITIALIZE_SIGNATURE_DATA", "    {");
                appendMacroLine("FUNCTIONS_INITIALIZE_SIGNATURE_MASK", "    {");
                foreach (var signature in signatures)
                {
                    (var pattern, var mask) = Helper.ProcessSignature(signature.Signature);
                    appendMacroLine("FUNCTIONS_INITIALIZE_SIGNATURE_DATA", $"        \"{pattern}\",");
                    appendMacroLine("FUNCTIONS_INITIALIZE_SIGNATURE_MASK", $"        \"{mask}\",");
                }
                appendMacroLine("FUNCTIONS_INITIALIZE_SIGNATURE_DATA", "    },");
                appendMacroLine("FUNCTIONS_INITIALIZE_SIGNATURE_MASK", "    },");

                //fill trampoline
                appendMacroLine("FUNCTIONS_TRAMPOLINES", $"{func.Name} PROC EXPORT");
                appendMacroLine("FUNCTIONS_TRAMPOLINES", $"  IFDEF RAX");
                appendMacroLine("FUNCTIONS_TRAMPOLINES", $"    jmp Functions_Addrs[{functionIndex}*8]");
                appendMacroLine("FUNCTIONS_TRAMPOLINES", $"  ELSE");
                appendMacroLine("FUNCTIONS_TRAMPOLINES", $"    jmp Functions_Addrs[{functionIndex}*4]");
                appendMacroLine("FUNCTIONS_TRAMPOLINES", $"  ENDIF");
                appendMacroLine("FUNCTIONS_TRAMPOLINES", $"{func.Name} ENDP");
                appendMacroLine("FUNCTIONS_TRAMPOLINES", "");

            }


            appendMacro("FUNCTIONS_SIGNATURES_MAXCOUNT", maxSignaturesCount.ToString());
            appendMacro("FUNCTIONS_COUNT", (functionIndex + 1).ToString());

        }

        void ProcessStructures()
        {

            int structureIndex = -1;
            int maxSignaturesCount = 0;

            foreach (var structure in db.GetStructures())
            {
                //get signature count
                var signatures = structure.GetSignatures(bitness);
                var signature_count = signatures.Count;

                if (signature_count == 0)
                    continue;

                if (maxSignaturesCount < signature_count)
                {
                    maxSignaturesCount = signature_count;
                }

                structureIndex++;

                appendMacroLine("STRUCTURES_NAMES", $"    structenum_{structure.Name}={structureIndex},");
                appendMacroLine("STRUCTURES_INITIALIZE_NAMES", $"    \"{structure.Name}\",");

                appendMacroLine("STRUCTURES_DECLARATION", $"extern \"C\" __declspec(dllexport) size_t {structure.Name} = 0;");
                
                appendMacroLine("STRUCTURES_FILL_TRAMPOLINES",  $"    {structure.Name}=Structures_Addrs[structenum_{structure.Name}];");

                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_COUNT", $"    {signature_count},");
                
                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_FUNCTIONINDEX", "    {");
                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_OFFSET", "    {");
                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_CHECKDATA", "    {");
                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_CHECKMASK", "    {");


                foreach (var sig in signatures)
                {
                    var tokens = sig.Signature.Split('+');
                    
                    appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_FUNCTIONINDEX", $"        funcenum_{tokens[0]},");
                    appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_OFFSET", $"        {tokens[1]},");
                    
                    (var pattern, var mask) = Helper.ProcessSignature(sig.ValidationPrefix);
                    appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_CHECKDATA", $"        \"{pattern}\",");
                    appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_CHECKMASK", $"        \"{mask}\",");
                }

                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_FUNCTIONINDEX", "    },");
                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_OFFSET", "    },");
                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_CHECKDATA", "    },");
                appendMacroLine("STRUCTURES_INITIALIZE_SIGNATURE_CHECKMASK", "    },");
            }

            appendMacro("STRUCTURES_SIGNATURES_MAXCOUNT", maxSignaturesCount.ToString());
            appendMacro("STRUCTURES_COUNT", (structureIndex + 1).ToString());
        }

        public void Generate()
        {
            if (!Directory.Exists(outputDir))
            {
                Directory.CreateDirectory(outputDir);
            }

            if (!Directory.Exists(Path.Combine(outputDir,bitness.ToString())))
            {
                Directory.CreateDirectory(Path.Combine(outputDir, bitness.ToString()));
            }

            foreach (var file in Directory.GetFiles(templateDir))
            {
                var fileContent = File.ReadAllText(file);
                foreach (var macro in macroses)
                {
                    fileContent = fileContent.Replace($"<<{macro.Key}>>", macro.Value.ToString());
                }

                
                File.WriteAllText(Path.Combine(outputDir,bitness.ToString(),Path.GetFileNameWithoutExtension(file)),fileContent);
            }
        }

    }
}
