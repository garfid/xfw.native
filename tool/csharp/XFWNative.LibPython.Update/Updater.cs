﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using XFWNative.Common.Extensions;
using XFWNative.Libpython.Common;

namespace XFWNative.Libpython.Update
{
    class Updater
    {
        #region Properties

        private Dictionary<string, List<string>> Log { get; } = new Dictionary<string, List<string>>();

        private string BinaryVersion = null;
        private string BinaryPath = null;
        private byte[] BinaryContent = null;

        private string DataDir { get; } = "";

        private DB Database { get; } = null;

        #endregion

        #region Initialization

        public Updater(string dataDir)
        {
            DataDir = dataDir;
            Database = new DB(DataDir);
            InitBinaries();

        }

        private void InitBinaries()
        {
            var clientDir = Directory.GetDirectories(Path.Combine(DataDir, "exe/")).Last();

            BinaryVersion = Path.GetFileName(clientDir);
            BinaryPath = Path.Combine(clientDir, "WorldOfTanks.exe");
            BinaryContent = File.ReadAllBytes(BinaryPath);

        }

        #endregion

        #region Processing

        public void Process()
        {
            Parallel.ForEach(Database.GetFunctions(), ProcessFunction);
            Parallel.ForEach(Database.GetStructures(), ProcessStructure);
            
            Database.SaveJsonData();
            PrintSummary();
        }

        private void ProcessFunction(Common.Data.Signatures.Function function)
        {
            //1. Check if signature exists
            if (!function.Signatures.Any() &&
                !Database.InlinedFunctions.Contains(function.Name))
            {
                LogMsg("function_signature_null", $"{function.Name}->any");
                return;
            }

            //2. Check if inlined
            if (function.Inlined)
            {
                if (function.Signatures.Any())
                {
                    LogMsg("function_inlined_with_signature", $"{function.Name}->any");
                    return;
                }
                else
                {
                    LogMsg("function_inlined_without_signature", $"{function.Name}->any");
                    return;
                }
            }

            //2. Check signature validity
            bool found = false;
            foreach (var signature in function.Signatures)
            {
                if (ProcessFunctionSignature(function, signature))
                    found = true;
            }

            if (!found)
                LogMsg("function_notfound", $"{function.Name}");

        }

        private bool ProcessFunctionSignature(Common.Data.Signatures.Function function,
            Common.Data.Signatures.FunctionSignature signature)
        {
            var indexes = BinaryContent.AllIndexesOf(signature.Signature);
            if (indexes.Length == 0)
            {
                return false;
            }

            if (indexes.Length > 1)
            {
                LogMsg("function_signature_multiplematch", $"{function.Name}/{signature.Signature} -> {BinaryVersion}");
                return false;
            }

            var ind = indexes[0] + 0x00400c00;
            var clientOffssets = Database.GetOffsetsForVersion(BinaryVersion);

            if (clientOffssets.ContainsKey(function.Name))
            {
                if (clientOffssets[function.Name] != "0x" + ind.ToString("X8"))
                {
                    LogMsg("function_offset_mismatch",
                        ($"{function.Name}->{BinaryVersion}: found {"0x" + ind.ToString("X8")}, stored {clientOffssets[function.Name]}"));
                }
            }
            else
            {
                clientOffssets[function.Name] = $"0x{ind:X8}";
            }

            return true;

        }

        
        private void ProcessStructure(Common.Data.Signatures.Structure structure)
        {
            //1. Check if signature exists
            if (!structure.Signatures.Any())
            {
                LogMsg("structure_signature_null", $"{structure.Name}");
                return;
            }

            bool found = false;
            foreach (var signature in structure.Signatures)
            {
                if (ProcessStructureSignature(structure, signature))
                    found = true;
            }
        }

        private bool ProcessStructureSignature(Common.Data.Signatures.Structure structure,
            Common.Data.Signatures.StructureSignature signature)
        {
            var tokens = signature.Signature.Split("+");
            if (tokens.Length != 2)
            {
                LogMsg("structure_signature_invalid", $"{structure.Name}/{signature.Signature}->any");
                return false;
            }

            var functionName = tokens[0];
            var offset = tokens[1];

            var clientOffssets = Database.GetOffsetsForVersion(BinaryVersion);

            if (!clientOffssets.ContainsKey(functionName))
            {
                LogMsg("structure_signature_error", $"{structure.Name}");
                return false;
            }

            var structureAddressBegin =
                Convert.ToInt32(clientOffssets[functionName], 16) + Convert.ToInt32(tokens[1], 16) - 0x00400c00;

            var structureAddress = BitConverter.ToInt32(BinaryContent, structureAddressBegin);

            if (!BinaryContent.CheckPrefix(structureAddressBegin, signature.ValidationPrefix))
            {
                LogMsg("structure_prefix_error", $"{structure.Name}");
                return false;
            }

            if (clientOffssets.ContainsKey(structure.Name))
            {
                if (clientOffssets[structure.Name] != "0x" + structureAddress.ToString("X8"))
                {
                    LogMsg("structure_offset_mismatch",  $"{structure.Name}->{BinaryVersion}: found {"0x" + structureAddress.ToString("X8")}, stored {clientOffssets[structure.Name]}");
                    return false;
                }
            }
            else
            {
                clientOffssets[structure.Name] = $"0x{structureAddress:X8}";
            }

            return true;
        }

        #endregion

        #region Results

        public void PrintSummary()
        {
            foreach (var error in Log)
            {
                error.Value.Sort();
                Console.WriteLine($"{error.Key}:");
                foreach (var line in error.Value)
                {
                    Console.WriteLine($"    {line}");
                }
                Console.WriteLine("");
            }
            Console.WriteLine("");
        }

        #endregion

        #region Logging

        private void LogMsg(string error, string text)
        {
            if (!Log.ContainsKey(error))
                Log[error] = new List<string>();

            Log[error].Add(text);
        }

        #endregion

    }
}
