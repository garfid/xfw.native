﻿using System.IO;
using System.Reflection;

namespace XFWNative.Libpython.Update
{
    class Program
    {
        static void Main(string[] args)
        {
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));

            var updater = new Updater("../../../../../data/lib_python/");

            updater.Process();
            
        }
    }
}
